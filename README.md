# Boxx.AI integration with Magento 2

[![N|Solid](https://boxx.ai/images/assets/LOGO.svg)](https://boxx.ai/)

Boxx.AI is most accurate , presonalized , relevant recommendation engine for e-commerce. Our world class recommendation engine can predict your customer's preferences accurately.
### Documentation
Check out the [Boxx.AI API Document](http://app.boxx.ai/client/sdk/#api-upload-data)
### Installation

Download latest version form git repository

