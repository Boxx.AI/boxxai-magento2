<?php
/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Cron\Cron;

use Magento\Framework\View\Element\Template;

class Syncdata extends Template
{

    const RECORDS_PER_PAGE = 100;
    const MEMORY_LIMIT = 0.7;

    /**
    *@var Magento\Store\Model\StoreRepository 
    */

    protected $_storeManager;

    /**
    * @var \Magento\Customer\Model\Customer
    */
    protected $_productCollectionFactory;
    
    /**
    * @var \Magento\Customer\Model\Customer
    */

    protected $_customers;

    /**
    * @var \Boxxai\Core\Helper\Data
    */

    protected $_apiConfig;

    /**
    * @var \Boxxai\Core\Api\UploadDataApi
    */
    protected $_uploadDataApi;

    /**
    * @var \Boxxai\Core\Model\UploadProduct
    */
    protected $_uploadProduct;

    /**
    * @var \Boxxai\Core\Model\UploadProductRequest
    */
    protected $_uploadProductRequest;

    /**
    * @var \Magento\CatalogInventory\Api\StockStateInterface
    */
    protected $stockStateInterface;

    /**
    * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFac‌​tory
    */
    protected $_customerCollectionFactory;

    /**
    * @var \Boxxai\Core\Model\UploadCustomer
    */
    protected $_uploadCustomer;

    /**
    * @var \Boxxai\Core\Model\UploadCustomerRequest 
    */
    protected $_uploadCustomerRequest;

    /**
    * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
    */
    protected $_orderCollectionFactory;

    /**
    * @var \Boxxai\Cron\Cron\Syncdata
    */
    protected $orders;

    /**
    * @var \Boxxai\Core\Model\UploadTransaction
    */
    protected $_uploadTransaction;

    /**
    * @var \Boxxai\Core\Model\UploadTransactionRequest 
    */
    protected $_uploadTransactionRequest;

    /**
    * @var \Magento\Catalog\Model\ResourceModel\Product\Action
    */
    protected $action;


    /**
    * @var \Psr\Log\LoggerInterface
    */
    protected $logger;


    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory */
    protected $_collectionFactory;


    /**@var \Magento\Eav\Model\Config*/
    protected $_eavConfig;

    /**@var \Magento\Framework\App\ResourceConnection */
    protected $_coreResource;

    /** @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable */

    protected $_configurableProductObj;

    /** @var \Magento\CatalogInventory\Api\StockRegistryInterface*/
    protected $_stockInterface;

    /**
    * Constructor
    *
    * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    * @param \Magento\Customer\Model\Customer $customers
    * @param \Boxxai\Core\Helper\Data $_apiConfig
    * @param \Boxxai\Core\Api\UploadDataApi $_uploadDataApi
    * @param \Boxxai\Core\Model\UploadProduct $_uploadProduct
    * @param \Boxxai\Core\Model\UploadProductRequest $_uploadProductRequest  
    * @param \Boxxai\Core\Model\UploadCustomer $_uploadCustomer
    * @param \Boxxai\Core\Model\UploadCustomerRequest $_uploadCustomerRequest
    * @param \Boxxai\Core\Model\UploadTransaction $_uploadTransaction,
    * @param \Boxxai\Core\Model\UploadTransactionRequest $_uploadTransactionRequest,
    * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    * @param \Magento\Catalog\Model\ResourceModel\Product\Action
    * @param \Psr\Log\LoggerInterface $logger
    */
    public function __construct(
        \Boxxai\Product\Model\ResourceModel\Product $productCollectionFactory,
        \Magento\Customer\Model\Customer $customers,
        \Boxxai\Core\Helper\Data $_apiConfig,
        \Boxxai\Core\Api\UploadDataApi $_uploadDataApi,
        \Boxxai\Core\Model\UploadProduct $_uploadProduct,
        \Boxxai\Core\Model\UploadProductRequest $_uploadProductRequest,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        \Boxxai\Core\Model\UploadCustomer $_uploadCustomer,
        \Boxxai\Core\Model\UploadCustomerRequest $_uploadCustomerRequest,        
        \Boxxai\Core\Model\UploadTransaction $_uploadTransaction,
        \Boxxai\Core\Model\UploadTransactionRequest $_uploadTransactionRequest,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\App\ResourceConnection $coreCollection,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProductObj,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockInterface

    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_apiConfig = $_apiConfig;
        $this->_uploadDataApi = $_uploadDataApi;
        $this->_uploadProduct = $_uploadProduct;
        $this->_uploadProductRequest = $_uploadProductRequest;
        $this->stockStateInterface = $stockStateInterface;
        $this->_uploadCustomer = $_uploadCustomer;
        $this->_uploadCustomerRequest = $_uploadCustomerRequest;
        $this->_customers = $customers;
        $this->_uploadTransaction = $_uploadTransaction;
        $this->_uploadTransactionRequest = $_uploadTransactionRequest;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_productSyncScope = \Boxxai\Core\Helper\Data::BOXXAI_PRODUCT_SYNC;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->logger = $logger;
        $this->_storeManager = $storeManager;
        $this->_collectionFactory = $collectionFactory;
        $this->_eavConfig = $eavConfig;
        $this->_coreResource = $coreCollection;
        $this->_configurableProductObj = $configurableProductObj;
        $this->_stockInterface = $stockInterface;
    }

    /**
     * Execute the cron
     *
     * @return void
     */

    /*## Unwanted ##*/
        // public function productFlagUpdate()
        // {
        //     // If account details are not present skip
        //     if($this->_apiConfig->getCampaignId()=="")
        //     {
        //         return;
        //     }
        //     // If Products Sync is active return
        //     if($this->_apiConfig->isProductSync()=="active")
        //     {
        //         return;
        //     }
        //     //Get Current Store Details

        //     //boxx_product_flag disabled
        //     /*$totalProducts = $this->_productCollectionFactory->addAttributeToSelect('*');
        //     $totalProducts->addAttributeToFilter(
        //         array(
        //             array('attribute'=>'boxx_product_flag','null' => true),
        //             array('attribute'=>'boxx_product_flag', 'neq' => '1'),
        //             array('attribute'=> 'boxx_product_flag','eq' => ''),
        //             array('attribute'=> 'boxx_product_flag','eq' => 'NO FIELD')
        //         ),
        //     '',
        //     'left'); 
        //     $totalProducts->getSelect()->limit(10000);  */

        //     $allProductIds=array();
        //     foreach ($totalProducts as $productid) {
        //         array_push($allProductIds, $productid->getId());
        //     }

        //     if(count($allProductIds)==0)
        //     {
        //         $this->logger->info('product sync flag set to active'.date('Y-m-d H:i:s'));
        //         //do nothing change productSync flag to active;
        //         $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_PRODUCT_SYNC,"active");
        //         $this->resetConfigCache();
        //         return;
        //     } 
            
        //     $this->logger->info('updating product flag'.date('Y-m-d H:i:s'));        

        //     //Create Object of Global Store
        //     $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //     // Get Store ID
        //     $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');             
        //     // Update Flag of products to 1
        //     /*$objectManager->get('Magento\Catalog\Model\ResourceModel\Product\Action')->updateAttributes($allProductIds, array('boxx_product_flag' => '1'),$storeManager->getStore()->getStoreId());*/
        //     return;
        // }


    /*## Unwanted ##*/
        // public function orderFlagUpdate()
        // {
        //     // If account details are not present skip
        //     if($this->_apiConfig->getCampaignId()=="")
        //     {
        //         return;
        //     }
        //     // If Order Sync is active return
        //     if($this->_apiConfig->isOrderSync()=="active")
        //     {
        //         return;
        //     }

        //     // Get Flag Updating Orders
        //     $orderFlagsToUpdate=$this->_orderCollectionFactory->create();
        //     $orderFlagsToUpdate->addAttributeToFilter('boxx_order_flag',[['neq'=>'1'],['null'=>true],['eq' => 'NO FIELD'],['eq' => '']]);
        //     $orderFlagsToUpdate->getSelect()->limit(5000);

        //     $allOrderIds=array();
        //     foreach($orderFlagsToUpdate as $orderids)   
        //     {
        //         array_push($allOrderIds, $orderids->getId());
        //     }

        //     if(count($allOrderIds)==0)
        //     {
        //         $this->logger->info('order sync flag set to active'.date('Y-m-d H:i:s'));
        //         //do nothing change productSync flag to active;
        //         $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_ORDER_SYNC,"active");
        //         $this->resetConfigCache();
        //         return;
        //     }
        
        //     $this->logger->info('updating order flag'.date('Y-m-d H:i:s'));
             
        //     foreach($orderFlagsToUpdate as $order)   
        //     {              
        //        $order->setData('boxx_order_flag', '1')->getResource()->saveAttribute($order, 'boxx_order_flag');  
        //     }   

        //     return;
        // }
    
    public function customerFlagUpdate()
    {
        // If account details are not present skip
        if($this->_apiConfig->getCampaignId()=="")
        {
            return;
        }
        // If Customer Sync is active return
        if($this->_apiConfig->isCustomerSync()=="active")
        {
            return;
        }

        // Get Total Customers
        $customerFlagsToUpdate=$this->_customers->getCollection();
        $customerFlagsToUpdate->addAttributeToFilter(
            array(
                array('attribute'=>'boxx_customer_flag','null' => true),
                array('attribute'=>'boxx_customer_flag', 'neq' => '1'),
                array('attribute'=> 'boxx_customer_flag','eq' => ''),
                array('attribute'=> 'boxx_customer_flag','eq' => 'NO FIELD')
            ),
        '',
        'left'); 
        $customerFlagsToUpdate->getSelect()->limit(5000); 
        
        $allCustomerIds=array();
        foreach($customerFlagsToUpdate as $customerids)   
        { 
            array_push($allCustomerIds, $customerids->getId());
        }

        if(count($allCustomerIds)==0)
        {
            $this->logger->info('customer sync flag set to active'.date('Y-m-d H:i:s'));
            //do nothing change productSync flag to active;
            $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_CUSTOMER_SYNC,"active");
            $this->resetConfigCache();
            return;
        }
        
        $this->logger->info('updating customer flag'.date('Y-m-d H:i:s'));
        
        foreach($customerFlagsToUpdate as $customer)   
        {    
           $customer->setData('boxx_customer_flag', '1')->getResource()->saveAttribute($customer, 'boxx_customer_flag');  
        }  
        return;
    }

    public function productSyncJobRun(){
        $stores = $this->_storeManager->getStores($withDefault = false);
        foreach ($stores as $store) {
            $this->productSyncJob($store);
        }
    }

    public function productSyncJob($store){

        try{
            /* Some logic that could throw an Exception */
            $this->logger->info('Starting Product Sync Proccess'.date('Y-m-d H:i:s'));

            $productCollection =  $this->_collectionFactory->create();

            $sync_query = array(
                'delete' => $this->_collectionFactory->create()->getSelect()
                    ->joinLeft(
                        array("ps" => $productCollection->getTable("boxx_productsync")),
                        "e.entity_id = ps.product_id","ps.parent_id"
                    )
                    ->joinLeft(
                        array('v' => $productCollection->getTable("catalog_category_product_index")),
                        "v.product_id = ps.product_id AND ps.store_id = {$store->getStoreId()}",
                        ""
                    )
                    ->joinLeft(
                        array('ss' => $this->getProductStatusAttribute()->getBackendTable()),
                        "ss.entity_id = e.entity_id",
                        "ss.value"
                    )
                    ->joinLeft(
                        array('st' => $productCollection->getTable("cataloginventory_stock_item")),
                        "st.product_id = e.entity_id",
                        ""
                    )->where("(ss.attribute_id = ".$this->getProductStatusAttribute()->getId().")
                        AND ((ss.value != 1)
                        OR (st.is_in_stock = 0))")
                    ->group(array('v.product_id'))
                ,
                'add' => $this->_collectionFactory->create()->getSelect()
                    ->joinLeft(
                        array('i' => $productCollection->getTable("catalog_category_product_index")),
                        "e.entity_id = i.product_id AND i.store_id = {$store->getStoreId()}",""
                    )
                    ->joinLeft(
                        array("ps" => $productCollection->getTable("boxx_productsync")),
                        "e.entity_id = ps.product_id",""
                    )
                    ->joinLeft(
                        array('s' => $productCollection->getTable("catalog_product_super_link")),
                        'e.entity_id = s.product_id', "s.parent_id"
                    )
                    ->joinLeft(
                        array('st' => $productCollection->getTable("cataloginventory_stock_item")),
                         "st.product_id = e.entity_id",
                         ""
                    )
                    ->joinLeft(
                        array('ss' => $this->getProductStatusAttribute()->getBackendTable()),
                        "ss.entity_id = e.entity_id",
                        "ss.value"
                    )
                    ->where("(ss.attribute_id = ".$this->getProductStatusAttribute()->getId().") 
                            AND (ss.value = 1)
                            AND (st.is_in_stock != 0)
                            AND (ps.product_id  IS NULL)
                            AND (e.type_id != 'configurable')")
                    ->group(array('e.entity_id'))
                ,
                'update' => $this->_collectionFactory->create()->getSelect()
                    ->joinLeft(
                        array("ps" => $productCollection->getTable("boxx_productsync")),
                        "e.entity_id = ps.product_id",""
                    )
                    ->joinLeft(
                        array('s' => $productCollection->getTable("catalog_product_super_link")),
                        'e.entity_id = s.product_id', "s.parent_id"
                    )
                    ->joinLeft(
                        array('st' => $productCollection->getTable("cataloginventory_stock_item")),
                         "st.product_id = e.entity_id",
                         ""
                    )
                    ->joinLeft(
                        array('ss' => $this->getProductStatusAttribute()->getBackendTable()),
                        "ss.entity_id = e.entity_id",
                        "ss.value"
                    )
                    ->where("(ss.attribute_id = ".$this->getProductStatusAttribute()->getId().") 
                            AND (ss.value = 1)
                            AND (e.updated_at > ps.last_synced_at)
                            AND (st.is_in_stock != 0)
                            AND (e.type_id != 'configurable')")
                    ->group(array('e.entity_id'))
            );

            //echo $sync_query['update']->__tostring(); exit;

            foreach ($sync_query as $action => $statement) {
                $connection = $this->_coreResource->getConnection();
                
                $method = $action . "Products";
                $products = $connection->fetchAll($statement);

                $total = count($products);
                $pages = ceil($total / static::RECORDS_PER_PAGE);

                for ($page = 1; $page <= $pages; $page++) {
                    $offset = ($page - 1) * static::RECORDS_PER_PAGE;
                    $this->logger->info($action. " ".date('Y-m-d H:i:s'));
                    if($action == "delete"){
                        $result = $this->syncProductsToBoxxai(array_slice($products, $offset, static::RECORDS_PER_PAGE), $store->getStoreId(), false);
                    }else{
                        $result = $this->syncProductsToBoxxai(array_slice($products, $offset, static::RECORDS_PER_PAGE), $store->getStoreId());                        
                    }

                    if($result){
                        $this->$method(array_slice($products, $offset, static::RECORDS_PER_PAGE), $store->getStoreId());
                    }
                }
            }

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }catch (\Error $e){
            $this->logger->info('exception in boxx syncdata cron:loop productSyncJob'.date('Y-m-d H:i:s'));
            $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        }
    }

    /**
     * Return the product status attribute model.
     *
     * @return Mage_Catalog_Model_Resource_Eav_Attribute
     */
    protected function getProductStatusAttribute() {
        if (!$this->hasData("status_attribute")) {
            $this->setData("status_attribute", $this->_eavConfig->getAttribute('catalog_product', 'status'));
        }

        return $this->getData("status_attribute");
    }

    protected function addProducts($products,$storeId){
        $this->logger->info('Add Product to the Product Sync Table'.date('Y-m-d H:i:s'));
        $connection = $this->_coreResource->getConnection();
        if(!empty($products)){
            foreach($products as $productKey => $productValue){

                $query = "replace into boxx_productsync"
                       . "(product_id, parent_id, store_id, last_synced_at) values"
                       . "(:product_id, :parent_id, :store_id, NOW())";

               $binds = array(
                    'product_id' => $productValue['entity_id'],
                    'parent_id' => ($productValue['parent_id'] ? $productValue['parent_id'] : $productValue['entity_id']),
                    'store_id' => $storeId
                );

               $connection->query($query, $binds);
            }
        }
    }

    protected function updateProducts($products,$storeId){
        $this->logger->info('Update Product to the Product Sync Table'.date('Y-m-d H:i:s'));
        $connection = $this->_coreResource->getConnection();
        if(!empty($products)){
            foreach($products as $productKey => $productValue){

                $query = "update boxx_productsync"
                        ." SET "
                        ."last_synced_at = Now() "
                        ."where product_id = :product_id "
                        ."AND parent_id = :parent_id "
                        ."AND store_id = :store_id";


                $binds = array(
                    'product_id' => $productValue['entity_id'],
                    'parent_id' => ($productValue['parent_id'] ? $productValue['parent_id'] : $productValue['entity_id']),
                    'store_id' => $storeId
                );

               $connection->query($query, $binds);
            }
        }
    }

    protected function deleteProducts($products,$storeId){
        $this->logger->info('Delete Product to the Product Sync Table'.date('Y-m-d H:i:s'));
        $connection = $this->_coreResource->getConnection();
        if(!empty($products)){
            foreach($products as $productKey => $productValue){

               $query = "delete from boxx_productsync"
                        ." where product_id = :product_id "
                        ."AND parent_id = :parent_id "
                        ."AND store_id = :store_id";


                $binds = array(
                    'product_id' => $productValue['entity_id'],
                    'parent_id' => ($productValue['parent_id'] ? $productValue['parent_id'] : $productValue['entity_id']),
                    'store_id' => $storeId
                );

               $connection->query($query, $binds);
            }
        }
    }

    /**
    * Execute product sync to boxx ai
    */

    public function syncProductsToBoxxai($products,$storeid,$status = true){
        $instanceData=array();
        $this->logger->info( "Product Sync Process started ".date('Y-m-d H:i:s'));
        $product_ids = implode(",",array_column($products, 'entity_id'));
        $_productCollection =  $this->_collectionFactory->create();
        $collection = $_productCollection->addAttributeToSelect('*')->addFieldToFilter('entity_id', array('in' => $product_ids));
        try{
            foreach ($collection as $_product) {
                $parentProduct = $this->_configurableProductObj->getParentIdsByChild($_product->getId());
                $stockStateInterface = $this->_stockInterface;
                $stockDetails=$stockStateInterface->getStockItem($_product->getId());
                $ProductStatus=false;
                $IsInStock=$stockDetails->getIsInStock();
                $getVisibility=(($_product->getVisibility())?$_product->getVisibility():"1");
                
                if($status){
                    if(($getVisibility=="1") or ($getVisibility=="3"))
                    {               
                        if($parentProduct AND (($_product->getTypeId()=="downloadable") or ($_product->getTypeId()=="virtual") or ($_product->getTypeId()=="simple")))
                        {
                            $ProductStatus=true;
                        }
                        else
                        {
                            $ProductStatus=false;
                        }
                    }
                    else
                    {
                        //status active
                        $ProductStatus=true;
                    }
                }else{
                    $ProductStatus=false;
                }

                // current product properties
                $currentProductProperties = array(
                "store_ids" => (($_product->getStoreIds())?$_product->getStoreIds():""),
                "boxx_parent_id" => (($parentProduct)?$parentProduct[0]:$_product->getId()),
                "name" => (($_product->getName())?$_product->getName():""),
                "sku"=>(($_product->getSku())?$_product->getSku():""),
                "price"=>(($_product->getFinalPrice())?$_product->getFinalPrice():""),
                "image_url"=>$this->_apiConfig->getMediaUrl().'catalog/product'.$_product->getImage(),
                "product_url"=>(($_product->getProductUrl())?$_product->getProductUrl():""),
                "category_ids"=>(($_product->getCategoryIds())?$_product->getCategoryIds():""),
                "qty"=>(($stockDetails->getQty())?$stockDetails->getQty():0),
                "description"=>(($_product->getDescription())?$_product->getDescription():""),
                "special_price"=>(($_product->getSpecialPrice())?$_product->getSpecialPrice():""),
                "weight"=>(($_product->getWeight())?$_product->getWeight():""),
                "color"=>(($_product->getColor())?$_product->getColor():""),
                "gender"=>(($_product->getGender())?$_product->getGender():""),
                "in_stock"=>(($IsInStock)?1:0),
                "product_type"=>$_product->getTypeId(),
                "visibility"=>(($_product->getVisibility())?$_product->getVisibility():"1"),
                "size"=>(($_product->getSize())?$_product->getSize():"")
                );

                $upload_product = new \Boxxai\Core\Model\UploadProduct();
                $upload_product->setProductid($_product->getId());
                $upload_product->setProperties($currentProductProperties);
                $upload_product->setActive($ProductStatus);
                array_push($instanceData,$upload_product);
            }

             $data = $instanceData;
            
            $data_format = array(
                "store_ids" => "string",
                "boxx_parent_id"=>"number",
                "name" => "string",
                "sku" => "string",
                "price" => "string",
                "image_url" => "string",
                "product_url" => "string",
                "category_ids" => "string",
                "qty" => "number",
                "description"=>"string",
                "special_price"=>"string",
                "weight"=>"string",
                "color"=>"string",
                "gender"=>"string",
                "in_stock"=>"number",
                "product_type"=>"string",
                "visibility" => "string",
                "size"=>"string"
            );

            $this->_uploadProductRequest->setClientId($this->_apiConfig->getClientId());
            $this->_uploadProductRequest->setAccessToken($this->_apiConfig->getAccessToken());
            $this->_uploadProductRequest->setData($data);
            $this->_uploadProductRequest->setDataFormat($data_format);

        } catch (\Exception $e) {
            $this->logger->info('exception in boxx syncdata cron:syncProductsToBoxxai'.date('Y-m-d H:i:s'));
            $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        } catch (\Error $e) {
            $this->logger->info('erorr in boxx syncdata cron:syncProductsToBoxxai '.date('Y-m-d H:i:s'));
            $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        }

        try {
            $result = $this->_uploadDataApi->apiUploadProductPut($this->_uploadProductRequest);
            $result = json_encode(json_decode($result));
            $result = json_decode($result,true);
            
            if($result["status"] == "200"){
                $this->logger->info('synced products'.date('Y-m-d H:i:s'));
                return true;
            }else{
                $this->logger->info('synced products status is not 200 '.date('Y-m-d H:i:s'));
                return false;
            }
        } catch (\Exception $e) {
            $this->logger->info('sync product exception'.date('Y-m-d H:i:s'));
            $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
            return false;
        }
    }

    /*## Unwanted ##*/
        // public function _syncProductsToBoxxai($product,$allProductIds){
        //     try {
        //         $instanceData=array();        
        //         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //         foreach($product as $_product)
        //         {
        //             try
        //             {
        //                 //fetch parent product id
        //                 $parentProduct = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($_product->getId());              
        //                 $stockStateInterface = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');               
        //                 $stockDetails=$stockStateInterface->getStockItem($_product->getId());
        //                 $ProductStatus=false;
        //                 $IsInStock=$stockDetails->getIsInStock();
        //                 $getVisibility=(($_product->getVisibility())?$_product->getVisibility():"1");
        //                 if($_product->getStatus()==2)
        //                 {
        //                     //status inactive is product status is disabled
        //                     $ProductStatus=false;
        //                 }
        //                 elseif(/*(intval($stockDetails->getQty())==0) or */($IsInStock==false))
        //                 {
        //                     /*if(($_product->getTypeId()=="configurable") or ($_product->getTypeId()=="grouped") or ($_product->getTypeId()=="bundle"))
        //                     {
        //                         $ProductStatus=true;
        //                     }
        //                     else
        //                     {       */              
        //                         //status inactive
        //                         $ProductStatus=false;
        //                     //}
        //                 }
        //                 elseif(($getVisibility=="1") or ($getVisibility=="3"))
        //                 {               
        //                     if($parentProduct AND (($_product->getTypeId()=="downloadable") or ($_product->getTypeId()=="virtual") or ($_product->getTypeId()=="simple")))
        //                     {
        //                         $ProductStatus=true;
        //                     }
        //                     else
        //                     {
        //                         $ProductStatus=false;
        //                     }
        //                 }
        //                 else
        //                 {
        //                     //status active
        //                     $ProductStatus=true;
        //                 }
        //                 // current product properties
        //                 $currentProductProperties = array(
        //                 "store_ids" => (($_product->getStoreIds())?$_product->getStoreIds():""),
        //                 "boxx_parent_id" => (($parentProduct)?$parentProduct[0]:$_product->getId()),
        //                 "name" => (($_product->getName())?$_product->getName():""),
        //                 "sku"=>(($_product->getSku())?$_product->getSku():""),
        //                 "price"=>(($_product->getFinalPrice())?$_product->getFinalPrice():""),
        //                 "image_url"=>$this->_apiConfig->getMediaUrl().'catalog/product'.$_product->getImage(),
        //                 "product_url"=>(($_product->getProductUrl())?$_product->getProductUrl():""),
        //                 "category_ids"=>(($_product->getCategoryIds())?$_product->getCategoryIds():""),
        //                 "qty"=>(($stockDetails->getQty())?$stockDetails->getQty():0),
        //                 "description"=>(($_product->getDescription())?$_product->getDescription():""),
        //                 "special_price"=>(($_product->getSpecialPrice())?$_product->getSpecialPrice():""),
        //                 "weight"=>(($_product->getWeight())?$_product->getWeight():""),
        //                 "color"=>(($_product->getColor())?$_product->getColor():""),
        //                 "gender"=>(($_product->getGender())?$_product->getGender():""),
        //                 "in_stock"=>(($IsInStock)?1:0),
        //                 "product_type"=>$_product->getTypeId(),
        //                 "visibility"=>(($_product->getVisibility())?$_product->getVisibility():"1"),
        //                 "size"=>(($_product->getSize())?$_product->getSize():"")
        //                 );
        //                 $upload_product = new \Boxxai\Core\Model\UploadProduct();
        //                 $upload_product->setProductid($_product->getId());
        //                 $upload_product->setProperties($currentProductProperties);
        //                 $upload_product->setActive($ProductStatus);
        //                 array_push($instanceData,$upload_product);
        //             }catch (\Exception $e){
        //                 $this->logger->info('exception in boxx syncdata cron:loop syncProductsToBoxxai'.date('Y-m-d H:i:s'));
        //                 $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        //             }catch (\Error $e){
        //                 $this->logger->info('exception in boxx syncdata cron:loop syncProductsToBoxxai'.date('Y-m-d H:i:s'));
        //                 $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        //             }
        //         }
        //         $data = $instanceData;
        //         $data_format = array(
        //             "store_ids" => "string",
        //             "boxx_parent_id"=>"number",
        //             "name" => "string",
        //             "sku" => "string",
        //             "price" => "string",
        //             "image_url" => "string",
        //             "product_url" => "string",
        //             "category_ids" => "string",
        //             "qty" => "number",
        //             "description"=>"string",
        //             "special_price"=>"string",
        //             "weight"=>"string",
        //             "color"=>"string",
        //             "gender"=>"string",
        //             "in_stock"=>"number",
        //             "product_type"=>"string",
        //             "visibility" => "string",
        //             "size"=>"string"
        //         );

        //         $this->_uploadProductRequest->setClientId($this->_apiConfig->getClientId());
        //         $this->_uploadProductRequest->setAccessToken($this->_apiConfig->getAccessToken());
        //         $this->_uploadProductRequest->setData($data);
        //         $this->_uploadProductRequest->setDataFormat($data_format);
        //     } catch (\Exception $e) {
        //         $this->logger->info('exception in boxx syncdata cron:syncProductsToBoxxai'.date('Y-m-d H:i:s'));
        //         $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        //     } catch (\Error $e) {
        //         $this->logger->info('erorr in boxx syncdata cron:syncProductsToBoxxai'.date('Y-m-d H:i:s'));
        //         $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        //     }
            
        //     try {
        //         $result = $this->_uploadDataApi->apiUploadProductPut($this->_uploadProductRequest);
        //         $this->logger->info('synced products'.date('Y-m-d H:i:s'));
        //     } catch (\Exception $e) {
        //         // Get Store ID
        //         $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        //         // Update Flag of products to 1
        //        /* $objectManager->get('Magento\Catalog\Model\ResourceModel\Product\Action')->updateAttributes($allProductIds, array('boxx_product_flag' => '1'),$storeManager->getStore()->getStoreId());*/
        //         $this->logger->info('sync product exception'.date('Y-m-d H:i:s'));
        //         $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        //     }
        //     return;
        // }

    /*## Unwanted ##*/
        // public function _productSyncJob()
        // {
        //     //no interaction if campaign id is incorrect
        //     if($this->_apiConfig->getCampaignId()=="")
        //     {
        //         return;
        //     }

        //     // If Customer Sync is active return
        //     if($this->_apiConfig->isProductSync()=="paused")
        //     {
        //         return;
        //     }
                    
           
        //     // get 250 products where boxx_product_flag is 0      
        //     $toBeSyncProducts = $this->_productCollectionFactory->addAttributeToSelect('*');
        //     /*$toBeSyncProducts->addAttributeToFilter('boxx_product_flag',array('eq' => '1'));*/
        //     $toBeSyncProducts->getSelect()->limit(250);

        //     $allProductIds=array();
        //     foreach ($toBeSyncProducts as $productids) {
        //         array_push($allProductIds, $productids->getId());
        //     }

        //     if(count($allProductIds)==0)
        //     {
        //     	return;
        //     }

        //     $this->logger->info('syncing products'.date('Y-m-d H:i:s'));
        //     //Create Object of Global Store
        //     $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //     // Get Store ID
        //     $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');             
        //     // Update Flag of products to 1
        //     /*$objectManager->get('Magento\Catalog\Model\ResourceModel\Product\Action')->updateAttributes($allProductIds, array('boxx_product_flag' => '2'),$storeManager->getStore()->getStoreId());*/
        //     $this->syncProductsToBoxxai($toBeSyncProducts,$allProductIds);
        //     return;
        // }

    public function customerSyncJob()
    {
        //no interaction if campaign id is incorrect
        if($this->_apiConfig->getCampaignId()=="")
        {
            return;
        }

        // If Customer Sync is active return
        if($this->_apiConfig->isCustomerSync()=="paused")
        {
            return;
        }

        // get 250 products where boxx_customer_flag is 0      
        $toBeSyncCustomers = $this->_customers->getCollection();
        $toBeSyncCustomers->addAttributeToFilter('boxx_customer_flag',array('eq' => '1'));
        $toBeSyncCustomers->getSelect()->limit(250);
        
        $allCustomerIds=array();

        foreach ($toBeSyncCustomers as $customer) {
            array_push($allCustomerIds, $customer->getId());
        }

        if(count($allCustomerIds)==0)
        {
        	return;
        }
                      
        $this->logger->info('syncing customers'.date('Y-m-d H:i:s'));
        
        foreach ($toBeSyncCustomers as $customer) {
            $customer->setData('boxx_customer_flag', '2')->getResource()->saveAttribute($customer, 'boxx_customer_flag');  
        }

        $this->syncCustomersToBoxxai($toBeSyncCustomers,$allCustomerIds);

        return;
    }

    public function orderSyncJobRun(){
        $stores = $this->_storeManager->getStores($withDefault = false);
        foreach ($stores as $store) {
            $this->orderSyncJob($store);
        }
    }

    public function orderSyncJob($store)
    {
        try{
            /* Some logic that could throw an Exception */
            $this->logger->info('Starting Order Sync Proccess'.date('Y-m-d H:i:s'));

            $orderCollection =  $this->_orderCollectionFactory->create();

            $sync_query = array(
                'add' => $this->_orderCollectionFactory->create()->getSelect()
                    ->joinLeft(
                        array("os" => $orderCollection->getTable("boxx_ordersync")),
                        "main_table.increment_id = os.order_id",""
                    )
                    ->where("(os.order_id IS NULL)")
                    ->group(array('main_table.increment_id'))
                    ->limit(200)
                );

            //echo $sync_query['add']->__tostring(); exit;

            foreach ($sync_query as $action => $statement) {
                $connection = $this->_coreResource->getConnection();
                
                $method = $action . "Orders";
                $orders = $connection->fetchAll($statement);

                $total = count($orders);
                $pages = ceil($total / static::RECORDS_PER_PAGE);

                for ($page = 1; $page <= $pages; $page++) {
                    $offset = ($page - 1) * static::RECORDS_PER_PAGE;
                    $this->logger->info($action. " ".date('Y-m-d H:i:s'));
                    $result = $this->syncOrdersToBoxxai(array_slice($orders, $offset, static::RECORDS_PER_PAGE), $store->getStoreId());

                    if($result){
                        $this->$method(array_slice($orders, $offset, static::RECORDS_PER_PAGE), $store->getStoreId());
                    }
                }
            }

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }catch (\Error $e){
            $this->logger->info('exception in boxx syncdata cron:loop ordersyncjob'.date('Y-m-d H:i:s'));
            $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        }
    }

    protected function addOrders($orders,$storeId){
        $this->logger->info('Add Order to the Order Sync Table'.date('Y-m-d H:i:s'));
        $connection = $this->_coreResource->getConnection();
        if(!empty($orders)){
            foreach($orders as $ordersKey => $ordersValue){
                $query = "replace into boxx_ordersync"
                        . "(order_id, store_id, last_synced_at) values"
                        . "(:increment_id, :store_id, NOW())";

                $binds = array(
                    'increment_id' => $ordersValue['increment_id'],
                    'store_id' => $storeId
                );

               $connection->query($query, $binds);
            }
        }
    }

    public function syncOrdersToBoxxai($orders,$storeid){
        $this->logger->info( "Order Sync Process started ".date('Y-m-d H:i:s'));
        $increment_ids = implode(",",array_column($orders, 'increment_id'));

        $toBeSyncOrder = $this->_orderCollectionFactory->create();
        $toBeSyncOrder->addAttributeToFilter('increment_id', array('in' => $increment_ids));

        $instanceData=array();
        foreach($toBeSyncOrder as $item){
           try
           {  
                if($item->getCustomerId()!="")
                {
                    if($item->getAllVisibleItems())
                    {
                        foreach($item->getAllVisibleItems() as $products)
                        {
                            // current order properties
                            $currentTransactionProperties = array(
                                    "id" => $item->getId()
                                );
                            $uploadTransactionData = new \Boxxai\Core\Model\UploadTransaction();
                            $uploadTransactionData->setTransactionid($item->getId()."_".$products->getProductId());
                            $uploadTransactionData->setCustomerid($item->getCustomerId());
                            $uploadTransactionData->setProductid($products->getProductId());
                            $uploadTransactionData->setType("OrderPlaced");
                            $uploadTransactionData->setTransactiondate($item->getCreatedAt());
                            $uploadTransactionData->setProperties($currentTransactionProperties);
                            array_push($instanceData,$uploadTransactionData);
                        } 
                    }                  
                }                
            }catch (\Exception $e){
                $this->logger->info('exception in boxx syncdata cron:loop ordersyncjob'.date('Y-m-d H:i:s'));
                $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
            }catch (\Error $e){
                $this->logger->info('exception in boxx syncdata cron:loop ordersyncjob'.date('Y-m-d H:i:s'));
                $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
            }
        }

        $data = $instanceData;
        $data_format = array(
                "id" => "number"
            );
        $this->_uploadTransactionRequest->setClientId($this->_apiConfig->getClientId());
        $this->_uploadTransactionRequest->setAccessToken($this->_apiConfig->getAccessToken());
        $this->_uploadTransactionRequest->setData($data);
        $this->_uploadTransactionRequest->setDataFormat($data_format);

        try { 
            if($data)
            {
                $result = $this->_uploadDataApi->apiUploadTransactionPut($this->_uploadTransactionRequest);
                $result = json_encode(json_decode($result));
                $result = json_decode($result,true);
                
                if($result["status"] == "200"){
                    $this->logger->info('synced orders '.date('Y-m-d H:i:s'));
                    return true;
                }else{
                    $this->logger->info('synced orders status is not 200 '.date('Y-m-d H:i:s'));
                    return false;
                }
            }else{
                return false;
            }
            $this->logger->info('synced orders'.date('Y-m-d H:i:s'));
        } catch (\Exception $e) {
            $this->logger->info('sync order exception'.date('Y-m-d H:i:s'));
            $this->logger->info($e->getMessage().date('Y-m-d H:i:s')); 
        }

    }

   /*## Unwanted ##*/
       //  public function _orderSyncJob()
       //  {
       //      //no interaction if campaign id is incorrect
       //      if($this->_apiConfig->getCampaignId()=="")
       //      {
       //          return;
       //      }

       //      // If Order Sync is active return
       //      if($this->_apiConfig->isOrderSync()=="paused")
       //      {
       //          return;
       //      }
                   
       //      // get 300 products where boxx_order_flag is 0      
       //      $toBeSyncOrder = $this->_orderCollectionFactory->create();
       //      $toBeSyncOrder->addAttributeToFilter('boxx_order_flag',array('eq' => '1'));
       //      $toBeSyncOrder->getSelect()->limit(200);
            
       //      $allOrderIds=array();
       //      foreach($toBeSyncOrder as $orderid){
       //          array_push($allOrderIds, $orderid->getId());
       //      }

       //      if(count($allOrderIds)==0)
       //      {
       //      	return;
       //      }

       //      $this->logger->info('syncing order'.date('Y-m-d H:i:s'));
            
       //      $instanceData=array();
       //      foreach($toBeSyncOrder as $item){
       //         try
    		 //   {
    			//    $item->setData('boxx_order_flag', '2')->getResource()->saveAttribute($item, 'boxx_order_flag');  
    			// 	if($item->getCustomerId()!="")
    			// 	{
    			// 		if($item->getAllVisibleItems())
    			// 		{
    			// 			foreach($item->getAllVisibleItems() as $products)
    			// 			{
    			// 				// current order properties
    			// 				$currentTransactionProperties = array(
    			// 						"id" => $item->getId()
    			// 					);
    			// 				$uploadTransactionData = new \Boxxai\Core\Model\UploadTransaction();
    			// 				$uploadTransactionData->setTransactionid($item->getId()."_".$products->getProductId());
    			// 				$uploadTransactionData->setCustomerid($item->getCustomerId());
    			// 				$uploadTransactionData->setProductid($products->getProductId());
    			// 				$uploadTransactionData->setType("OrderPlaced");
    			// 				$uploadTransactionData->setTransactiondate($item->getCreatedAt());
    			// 				$uploadTransactionData->setProperties($currentTransactionProperties);
    			// 				array_push($instanceData,$uploadTransactionData);
    			// 			} 
    			// 		}                  
    			// 	}                
    			// }catch (\Exception $e){
    			// 	$this->logger->info('exception in boxx syncdata cron:loop ordersyncjob'.date('Y-m-d H:i:s'));
    			// 	$this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
    			// }catch (\Error $e){
    			// 	$this->logger->info('exception in boxx syncdata cron:loop ordersyncjob'.date('Y-m-d H:i:s'));
    			// 	$this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
    			// }
       //      }
            
       //      $data = $instanceData;
       //      $data_format = array(
       //              "id" => "number"
       //          );
       //      $this->_uploadTransactionRequest->setClientId($this->_apiConfig->getClientId());
       //      $this->_uploadTransactionRequest->setAccessToken($this->_apiConfig->getAccessToken());
       //      $this->_uploadTransactionRequest->setData($data);
       //      $this->_uploadTransactionRequest->setDataFormat($data_format);

       //      try { 
       //          if($data)
    			// {
    			// 	$result = $this->_uploadDataApi->apiUploadTransactionPut($this->_uploadTransactionRequest);
    			// }
       //          $this->logger->info('synced orders'.date('Y-m-d H:i:s'));
       //      } catch (\Exception $e) {
       //          foreach($toBeSyncOrder as $item){
       //              $item->setData('boxx_order_flag', '1')->getResource()->saveAttribute($item, 'boxx_order_flag'); 
       //          }
       //          $this->logger->info('sync order exception'.date('Y-m-d H:i:s'));
       //          $this->logger->info($e->getMessage().date('Y-m-d H:i:s')); 
       //      }   
       //  }

    /**
     * reset config cache
     *
     * @return $bool;
     */ 

    public function resetConfigCache()
    {
        $this->_cacheTypeList->cleanType('config');     
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
        return;
    }

    /**
    * Execute customer sync to boxx ai
    */ 

    public function syncCustomersToBoxxai($_customer,$allCustomerIds){
        $instanceData=array();
        foreach($_customer as $customer)
        {
            $customerProperties = array(
                "firstname" => $customer->getFirstName(),
                "lastname" => $customer->getLastName()
            );
            $UploadCustomerData=new \Boxxai\Core\Model\UploadCustomer();
            $UploadCustomerData->setCustomerid($customer->getId());
            $UploadCustomerData->setProperties($customerProperties);
            $UploadCustomerData->setEmail($customer->getEmail()); // optional
            array_push($instanceData,$UploadCustomerData);
        }

        $data = $instanceData;
        
        $data_format = array(
            "firstname" => "string",
            "lastname" => "string"
        );
     
        $this->_uploadCustomerRequest->setClientId($this->_apiConfig->getClientId());
        $this->_uploadCustomerRequest->setAccessToken($this->_apiConfig->getAccessToken());
        $this->_uploadCustomerRequest->setData($data);
        $this->_uploadCustomerRequest->setDataFormat($data_format);

        try {
            $result = $this->_uploadDataApi->apiUploadCustomerPut($this->_uploadCustomerRequest);
            $this->logger->info('synced customers'.date('Y-m-d H:i:s'));
        } catch (\Exception $e) {
            foreach($_customer as $customer)
            {
                $customer->setData('boxx_customer_flag', '1')->getResource()->saveAttribute($customer, 'boxx_customer_flag');  
            }
            $this->logger->info('sync customer exception'.date('Y-m-d H:i:s'));
            $this->logger->info($e->getMessage().date('Y-m-d H:i:s'));
        }
    }

    public function markAllProductsForUpdate($store) {
        $this->logger->info('Update All Product to the Product Sync Table'.date('Y-m-d H:i:s'));
        $connection = $this->_coreResource->getConnection();

        $query = "update boxx_productsync"
                ." SET "
                ."last_synced_at = 0 "
                ."where store_id = :store_id";


        $binds = array(
                'store_id' => $store->getStoreId()
        );

       $connection->query($query, $binds);

    }
   
}
