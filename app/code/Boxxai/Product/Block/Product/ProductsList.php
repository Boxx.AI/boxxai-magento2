<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Product\Block\Product;

use Boxxai\Core\Helper\Data as BoxxHelperData;
use Boxxai\Magic\Api\RecommendationApi as BoxxMagicRecommendationApi;
use Boxxai\Magic\Model\RecommendationRequestData as BoxxMagicRecommendationRequestData;
use Boxxai\Magic\Model\RecommendationRequestDataQuery as BoxxMagicRecommendationRequestDataQuery;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Design\Theme\ThemeProviderInterface;

/**
 * Catalog Products List widget block
 * Class ProductsList
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductsList extends AbstractProduct implements IdentityInterface {
	const DEFAULT_PRODUCTS_COUNT = 10;
	const PAGE_VAR_NAME = 'np';
	const DEFAULT_PRODUCTS_PER_PAGE = 5;
	const DEFAULT_SHOW_PAGER = false;
	protected $pager;
	protected $httpContext;
	protected $catalogProductVisibility;
	protected $productCollectionFactory;
	private $priceCurrency;
	protected $_apiConfig;
	protected $_recommendationApi;
	protected $_recommendationRequestData;
	protected $_recommendationRequestDataQuery;
	protected $_session;
	protected $customerSession;
	protected $_checkoutSession;
	protected $_scopeConfig;
	protected $_storeManager;
	protected $_themeProvider;
	protected $logger;

	public function __construct(
		Context $context,
		CollectionFactory $productCollectionFactory,
		Visibility $catalogProductVisibility,
		HttpContext $httpContext,
		BoxxHelperData $_apiConfig,
		BoxxMagicRecommendationApi $_recommendationApi,
		BoxxMagicRecommendationRequestData $_recommendationRequestData,
		BoxxMagicRecommendationRequestDataQuery $_recommendationRequestDataQuery,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Checkout\Model\Session $checkoutSession,
		ThemeProviderInterface $ThemeProvider,
		array $data = []
	) {
		parent::__construct(
			$context,
			$data
		);
		$this->_apiConfig = $_apiConfig;
		$this->_recommendationApi = $_recommendationApi;
		$this->_recommendationRequestData = $_recommendationRequestData;
		$this->_recommendationRequestDataQuery = $_recommendationRequestDataQuery;
		$this->_session = $context->getSession();
		$this->customerSession = $customerSession;
		$this->_checkoutSession = $checkoutSession;
		$this->_scopeConfig = $context->getScopeConfig();
		$this->_storeManager = $context->getStoreManager();
		$this->logger = $context->getLogger();
		$this->_themeProvider = $ThemeProvider;
		$this->productCollectionFactory = $productCollectionFactory;
		$this->catalogProductVisibility = $catalogProductVisibility;
		$this->httpContext = $httpContext;

	}

	/**
	 * {@inheritdoc}
	 */
	protected function _construct() {
		parent::_construct();
		$this->addColumnCountLayoutDepend('empty', 6)
			->addColumnCountLayoutDepend('1column', 5)
			->addColumnCountLayoutDepend('2columns-left', 4)
			->addColumnCountLayoutDepend('2columns-right', 4)
			->addColumnCountLayoutDepend('3columns', 3);

		$this->addData([
			'cache_lifetime' => false,
			'cache_tags' => [\Magento\Catalog\Model\Product::CACHE_TAG,
			]]);

		$layout = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\View\LayoutInterface');
		$layout->getUpdate()->addHandle('default');
	}

	/**
	 * Get key pieces for caching block content
	 *
	 * @return array
	 */

	public function getCacheKeyInfo() {
		return [];
		$conditions = $this->getData('conditions')
		? $this->getData('conditions')
		: $this->getData('conditions_encoded');

		return [
			'CATALOG_PRODUCTS_LIST_WIDGET',
			$this->getPriceCurrency()->getCurrencySymbol(),
			$this->_storeManager->getStore()->getId(),
			$this->_design->getDesignTheme()->getId(),
			$this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_GROUP),
			intval($this->getRequest()->getParam($this->getData('page_var_name'), 1)),
			$this->getProductsPerPage(),
			$conditions,
			json_encode($this->getRequest()->getParams()),
		];

	}

	/**
	 * {@inheritdoc}
	 */
	protected function _beforeToHtml() {
		$this->setProductCollection($this->createCollection());

		/*Get Theme Details*/
		$themeId = $this->_scopeConfig->getValue(
			\Magento\Framework\View\DesignInterface::XML_PATH_THEME_ID,
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->_storeManager->getStore()->getId()
		);
		$theme = $this->_themeProvider->getThemeById($themeId);
		$themeDetails = $theme->getData();

		$dirobjectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$directory = $dirobjectManager->get('\Magento\Framework\Filesystem\DirectoryList');

		$templateFolder = $directory->getPath('app') . "/design/frontend/" . $themeDetails['theme_path'] . "/Magento_Catalog/templates/";

		$defaultTemplate = "product/widget/new/content/new_grid.phtml";
		$boxxWidgetCustTemplateFile = $this->getWidgetCustTemplateFile();
		if ($boxxWidgetCustTemplateFile != "") {
			$boxxTemplate = "product/widget/boxx/content/" . $boxxWidgetCustTemplateFile . ".phtml";
		} else {
			$boxxTemplate = "product/widget/boxx/content/grid.phtml";
		}

		if ($themeDetails['theme_path'] == "Magento/luma") {
			$this->setTemplate("product/widget/content/grid.phtml");
		} elseif (file_exists($templateFolder . $boxxTemplate)) {
			$this->setTemplate($boxxTemplate);
		} elseif (file_exists($templateFolder . $defaultTemplate)) {
			$this->setTemplate($defaultTemplate);
		} else {
			$this->logger->info('template file not found' . date('Y-m-d H:i:s'));
			return '';
		}

		return parent::_beforeToHtml();
	}

	/**
	 * Prepare and return product collection
	 *
	 * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
	 */
	public function createCollection() {
		/**
		 * Prevent retrieval of duplicate records. This may occur when multiselect product attribute matches
		 * several allowed values from condition simultaneously
		 */

		if ($this->_apiConfig->isProductSync() != "active") {
			return;
		}

		if ($this->getWidgetStatusBoxx() == "disable") {
			return;
		}

		$widget_type = $this->getDisplayType();
		$widget_instance = $this->getWidgetTrackingCode();
		$page_url = $this->getCurrentUrl();

		$optimise_type = 'View';
		$validateResponse = $this->_apiConfig->getValidateClient($optimise_type);
		$responseArray = json_decode($validateResponse['response'], true);

		if ($responseArray['data']['valid']) {
			//recommendation request data query
			if ($this->customerSession->isLoggedIn()) {
				$customerId = $this->customerSession->getCustomer()->getId();
				$this->_recommendationRequestDataQuery->setUserid($customerId);
			} else {
				if ($this->_session->getBoxxTokenId()) {
					$this->_recommendationRequestDataQuery->setBoxxTokenId($this->_session->getBoxxTokenId());
				} else {
					return;
				}
			}

			$itemFilter = array('store_ids' => $this->_storeManager->getStore()->getId());
			if ($this->getDisplayType() == 'product_page') {

				$productId = $this->getProductIds();
				$categories = $this->getProductCats();
				//category_ids is the property stored in db
				if (!empty($categories)) {
					$itemFilter['category_ids'] = $categories[0];
				}
				$this->_recommendationRequestDataQuery->setRelatedProducts(array($productId));
				$this->_recommendationRequestDataQuery->setRelatedActionType('ProductView');
			}

			if ($this->getDisplayType() == 'cart_page') {
				$quote = $this->_checkoutSession->getQuote();
				$items = $quote->getAllItems();
				$cartProductIds = [];
				foreach ($items as $item) {
					$cartProductIds[] = $item->getProductId();
				}

				$this->_recommendationRequestDataQuery->setRelatedProducts($cartProductIds);
				$this->_recommendationRequestDataQuery->setRelatedActionType('OrderPlaced');
			}
			$this->_recommendationRequestDataQuery->setNum((int) $this->getProductsCount());
			$this->_recommendationRequestDataQuery->setItemFilters($itemFilter);

			//recommendation request data
			$this->_recommendationRequestData->setClientId($responseArray['data']['client_id']);
			$this->_recommendationRequestData->setChannelId($responseArray['data']['channel_id']);
			$this->_recommendationRequestData->setAccessToken($responseArray['data']['access_token']);
			$this->_recommendationRequestData->setQuery($this->_recommendationRequestDataQuery);
			//Tracking Data
			$this->_recommendationRequestData->setWidgetInstance($widget_instance);
			$this->_recommendationRequestData->setWidgetType($widget_type);
			$this->_recommendationRequestData->setPageUrl($page_url);
			$recommendedProducts = array();
			$result = array();
			try {
				$result = $this->_recommendationApi->rootPost($this->_recommendationRequestData);
				if (!empty($result->getResult())) {
					foreach ($result->getResult() as $key => $value) {
						array_push($recommendedProducts, $value->id);
					}
				}

			} catch (\Exception $e) {
				return;
			}

			/** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
			$collection = $this->productCollectionFactory->create();
			$collection->addAttributeToFilter('entity_id', array(' in' => $recommendedProducts));
			$collection = $this->_addProductAttributesAndPrices($collection);
			$collection->setStore($this->_storeManager->getStore()->getId())
				->addUrlRewrite()
				->addStoreFilter($this->_storeManager->getStore()->getId())
				->setPageSize($this->getPageSize())
				->setCurPage($this->getRequest()->getParam($this->getData('page_var_name'), 1));
			if (!empty($recommendedProducts)) {
				$collection->getSelect()->reset('order');
				$collection->getSelect()->order(new \Zend_Db_Expr('FIELD(e.entity_id, ' . implode(',', $recommendedProducts) . ')'));
				return $collection;
			} else {
				return;
			}
		} else {
			return;
		}
	}

	/**
	 * Retrieve how many products should be displayed
	 *
	 * @return int
	 */
	public function getProductsCount() {
		if ($this->hasData('products_count')) {
			return $this->getData('products_count');
		}

		if (null === $this->getData('products_count')) {
			$this->setData('products_count', self::DEFAULT_PRODUCTS_COUNT);
		}

		return $this->getData('products_count');
	}

	/**
	 * Retrieve how many products should be displayed
	 *
	 * @return int
	 */
	public function getProductsPerPage() {
		if (!$this->hasData('products_per_page')) {
			$this->setData('products_per_page', self::DEFAULT_PRODUCTS_PER_PAGE);
		}
		return $this->getData('products_per_page');
	}

	/**
	 * Return flag whether pager need to be shown or not
	 *
	 * @return bool
	 */
	public function showPager() {
		if (!$this->hasData('show_pager')) {
			$this->setData('show_pager', self::DEFAULT_SHOW_PAGER);
		}
		return (bool) $this->getData('show_pager');
	}

	/**
	 * Retrieve how many products should be displayed on page
	 *
	 * @return int
	 */
	protected function getPageSize() {
		return $this->showPager() ? $this->getProductsPerPage() : $this->getProductsCount();
	}

	/**
	 * Render pagination HTML
	 *
	 * @return string
	 */
	public function getPagerHtml() {
		if ($this->showPager() && $this->getProductCollection()->getSize() > $this->getProductsPerPage()) {
			if (!$this->pager) {
				$this->pager = $this->getLayout()->createBlock(
					\Magento\Catalog\Block\Product\Widget\Html\Pager::class,
					'widget.products.list.pager'
				);

				$this->pager->setUseContainer(true)
					->setShowAmounts(true)
					->setShowPerPage(false)
					->setPageVarName($this->getData('page_var_name'))
					->setLimit($this->getProductsPerPage())
					->setTotalLimit($this->getProductsCount())
					->setCollection($this->getProductCollection());
			}
			if ($this->pager instanceof \Magento\Framework\View\Element\AbstractBlock) {
				return $this->pager->toHtml();
			}
		}
		return '';
	}

	public function getProductPriceHtml(
		\Magento\Catalog\Model\Product $product,
		$priceType = null,
		$renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
		array $arguments = []
	) {
		if (!isset($arguments['zone'])) {
			$arguments['zone'] = $renderZone;
		}
		$arguments['price_id'] = isset($arguments['price_id'])
		? $arguments['price_id']
		: 'old-price-' . $product->getId() . '-' . $priceType;
		$arguments['include_container'] = isset($arguments['include_container'])
		? $arguments['include_container']
		: true;
		$arguments['display_minimal_price'] = isset($arguments['display_minimal_price'])
		? $arguments['display_minimal_price']
		: true;

		/** @var \Magento\Framework\Pricing\Render $priceRender */
		$priceRender = $this->getLayout()->getBlock('product.price.render.default');

		$price = '';
		if ($priceRender) {
			$price = $priceRender->render(
				\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
				$product,
				$arguments
			);
		}
		return $price;
	}

	/**
	 * Return identifiers for produced content
	 *
	 * @return array
	 */
	public function getIdentities() {
		$identities = [];
		if ($this->getProductCollection()) {
			foreach ($this->getProductCollection() as $product) {
				if ($product instanceof IdentityInterface) {
					$identities = array_merge($identities, $product->getIdentities());
				}
			}
		}

		return $identities ?: [\Magento\Catalog\Model\Product::CACHE_TAG];
	}

	/**
	 * Get value of widgets' title parameter
	 *
	 * @return mixed|string
	 */
	public function getHeading() {
		return $this->getData('title');
	}

	/**
	 * @return PriceCurrencyInterface
	 *
	 * @deprecated 100.2.0
	 */
	private function getPriceCurrency() {
		if ($this->priceCurrency === null) {
			$this->priceCurrency = \Magento\Framework\App\ObjectManager::getInstance()
				->get(PriceCurrencyInterface::class);
		}
		return $this->priceCurrency;
	}
}
