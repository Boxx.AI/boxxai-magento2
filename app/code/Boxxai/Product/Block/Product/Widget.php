<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Product\Block\Product;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Catalog\Block\Product\Context;
use Boxxai\Core\Helper\Data as BoxxHelperData;


/**
 * Catalog Products List widget block
 * Class ProductsList
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Widget extends Template implements BlockInterface
{
    protected $_apiConfig;
	protected $urlBuider;
	protected $_registry;
	

    public function __construct(
        Context $context,
        BoxxHelperData $_apiConfig,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );
		$this->_apiConfig = $_apiConfig;
		$this->urlBuilder = $context->getUrlBuilder();
        $this->_registry = $context->getRegistry();
       
    }
	
	public function _toHtml(){	
		
		$setWidgetStatus=$this->getWidgetStatusBoxx();
        $setWidgetTrackingCode=$this->getWidgetTrackingCode();
        $setWidgetCustTemplateFile=$this->getTemplate();
        $setWidgetType=$this->getDisplayType();
        $setWidgetTitle=$this->getData('title');
        $setWidgetCount=(int)$this->getProductsCount();
        $setWidgetPager=$this->getData('show_pager');
		
		$url = \Magento\Framework\App\ObjectManager::getInstance()
        ->get('Magento\Framework\UrlInterface');
		
		
		$currentProduct=null;
		if($setWidgetType == 'product_page'){
			$currentProduct = $this->_registry->registry('current_product');
		}
		
		
        $widget_settings=array(
            "setWidgetStatus"=>$setWidgetStatus,
            "setWidgetTrackingCode"=>$setWidgetTrackingCode,
            "setWidgetType"=>$setWidgetType,
            "setWidgetTitle"=>$setWidgetTitle,
            "setWidgetPager"=>$setWidgetPager,
            "setWidgetCount"=>$setWidgetCount,
            "setWidgetCustTemplateFile"=>$setWidgetCustTemplateFile,
            "setCurrentUrl"=>$url->getCurrentUrl(),
            "setproductRegistryId"=>(($currentProduct)?$currentProduct->getId():""),
            "setproductRegistryCat"=>(($currentProduct)?json_encode($currentProduct->getCategoryIds()):"")
        );
		
		$post_widget_settings=base64_encode(json_encode($widget_settings));
		
		if ($setWidgetStatus=='disable') {
            return '';
        }
		
		$magicUrl=$this->urlBuilder->getUrl('boxxaiproduct/index/getmagic', ['_secure' => true]);
		
		$magic_id=rand(pow(10, 3-1), pow(10, 3)-1);
        $data= '
        <div id="magic_reccos_'.$magic_id.'"></div>      
        <script type="text/javascript">		
			var Widget_Details={url:"'.$magicUrl.'?widget_settings='.$post_widget_settings.'",divid:"#magic_reccos_'.$magic_id.'"};
			BOXX_WIDGETS.push(Widget_Details);
        </script>
        ';
		return $data;
	}

}
