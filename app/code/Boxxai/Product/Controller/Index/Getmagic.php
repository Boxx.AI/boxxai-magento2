<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Product\Controller\Index;

class Getmagic extends \Magento\Framework\App\Action\Action
{
    protected $_session;
    protected $_apiConfig;
    protected $sessionManager;
    protected $action;
    protected $logger;
    protected $context;
    protected $productlist;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
    * @param \Boxxai\Core\Helper\Data $_apiConfig
    * @param \Magento\Catalog\Model\ResourceModel\Product\Action
    * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Boxxai\Core\Helper\Data $_apiConfig,
        \Boxxai\Product\Block\Product\ProductsList $productlist,
        \Magento\Catalog\Model\ResourceModel\Product\Action $action,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_apiConfig = $_apiConfig;
        $this->_action = $action;
        $this->productlist = $productlist;
        $this->logger = $logger;
        $this->_session = $sessionManager;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
    */

    public function execute()
    {   
		$widget_settings=(($this->getRequest()->getParam('widget_settings'))?$this->getRequest()->getParam('widget_settings'):null);
   		if($widget_settings==null)
   		{
   			return;
   		}
   		else
   		{			
   			// Load Widget
   			$get_widget_settings=json_decode(base64_decode($widget_settings));
			$this->getResponse()->setBody(
				$this->productlist
				->setData('widget_status_boxx',$get_widget_settings->setWidgetStatus)
				->setData('widget_tracking_code',$get_widget_settings->setWidgetTrackingCode)
				->setData('display_type',$get_widget_settings->setWidgetType)
				->setData('title',$get_widget_settings->setWidgetTitle)
				->setData('products_count',$get_widget_settings->setWidgetCount)
				->setData('current_url',$get_widget_settings->setCurrentUrl)
				->setData('show_pager',$get_widget_settings->setWidgetPager)
				->setData('widget_cust_template_file',$get_widget_settings->setWidgetCustTemplateFile)
				->setData('product_ids',$get_widget_settings->setproductRegistryId)
				->setData('product_cats',json_decode($get_widget_settings->setproductRegistryCat))
				->toHtml()
			);
   		}
    }
	protected function _isAllowed(){
		return true;
    }

}
