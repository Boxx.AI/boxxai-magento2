<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Product\Observer\Catalog;

class ProductAttributeUpdateBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
    * @var \Boxxai\Core\Api\UploadDataApi
    */
    protected $_uploadDataApi;

    /**
    * @var \Boxxai\Core\Model\UploadProduct
    */
    protected $_uploadProduct;

    /**
    * @var \Boxxai\Core\Model\UploadProductRequest
    */
    protected $_uploadProductRequest;

    /**
    * @var \Boxxai\Core\Helper\Data
    */
    protected $_apiConfig;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /**
    * @var \Magento\CatalogInventory\Api\StockStateInterface
    */
    protected $stockStateInterface;

    /**
    *
    * @param \Boxxai\Core\Helper\Data $_apiConfig
    * @param \Boxxai\Core\Api\UploadDataApi $_uploadDataApi
    * @param \Boxxai\Core\Model\UploadProduct $_uploadProduct
    * @param \Boxxai\Core\Model\UploadProductRequest $_uploadProductRequest
    * @param \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Boxxai\Core\Helper\Data $_apiConfig,
        \Boxxai\Core\Api\UploadDataApi $_uploadDataApi,
        \Boxxai\Core\Model\UploadProduct $_uploadProduct,
        \Boxxai\Core\Model\UploadProductRequest $_uploadProductRequest,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_uploadDataApi = $_uploadDataApi;
        $this->_uploadProduct = $_uploadProduct;
        $this->_uploadProductRequest = $_uploadProductRequest;
        $this->stockStateInterface = $stockStateInterface;
        $this->_apiConfig = $_apiConfig;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        //no interaction if campaign id is incorrect
        if($this->_apiConfig->getCampaignId()=="")
        {
            return;
        }
        $productIds = $observer->getProductIds();
        $attributedata = $observer->getAttributesData();
        // if(array_key_exists("boxx_product_flag", $attributedata))
        // {
        //    //do nothing
        // }
        // elseif(!empty($productIds)){
        //     //Create Object of Global Store
        //     $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //     // Get Store ID
        //     $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');             
        //     // Update Flag of products to 1
        //     $objectManager->get('Magento\Catalog\Model\ResourceModel\Product\Action')->updateAttributes($productIds, array('boxx_product_flag' => '1'),$storeManager->getStore()->getStoreId());
        // }
        //die;
    }
}