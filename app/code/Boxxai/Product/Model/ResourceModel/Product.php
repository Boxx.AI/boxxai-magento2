<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Product\Model\ResourceModel;

class Product extends \Magento\Catalog\Model\ResourceModel\Product\Collection
{   
	public function isEnabledFlat()
    {        
        return false;
    }
}