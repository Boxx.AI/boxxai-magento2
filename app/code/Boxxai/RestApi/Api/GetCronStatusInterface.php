<?php
namespace Boxxai\RestApi\Api;

interface GetCronStatusInterface
{
    /**
     * Returns Cron status
     *
     * @api
     * @return bool.
     */
    public function getCronStatus();
}