<?php
namespace Boxxai\RestApi\Api;

interface GetBoxxWidgetInstancesInterface
{
    /**
     * Returns greeting message to user
     *
     * @api
     * @return string.
     */
    public function getBoxxWidgetInstances();
}