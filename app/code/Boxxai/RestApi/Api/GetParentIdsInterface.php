<?php
namespace Boxxai\RestApi\Api;
 
interface GetParentIdsInterface
{
    /**
     * Returns greeting message to user
     *
     * @api
     * @param mixed $products Users name.
     * @return mixed Greeting message with products.
     */
    public function getParentIds($products);
}