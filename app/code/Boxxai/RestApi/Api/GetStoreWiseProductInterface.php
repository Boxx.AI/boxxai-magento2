<?php
namespace Boxxai\RestApi\Api;

interface GetStoreWiseProductInterface
{
    /**
     * Returns greeting message to user
     *
     * @api
     * @return string.
     */
    public function getStoreWiseProduct();
}