<?php
namespace Boxxai\RestApi\Model;
use Boxxai\RestApi\Api\GetBoxxWidgetInstancesInterface;

class GetBoxxWidgetInstances implements GetBoxxWidgetInstancesInterface
{

    const BLOCK_PRODUCT_WIDGET = "Boxxai\Product\Block\Product\Widget";

    const XML_PATH_CLIENT = 'boxxai_section/boxxai_group/client_id';

    /** @var \Magento\Widget\Model\ResourceModel\Widget\Instance\CollectionFactory*/
    protected $_collectionFactory;

    protected $_storeManager;

    /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
   protected $scopeConfig;

    public function __construct(
        \Magento\Widget\Model\ResourceModel\Widget\Instance\CollectionFactory $CollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_collectionFactory = $CollectionFactory;
        $this->_storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Returns greeting message to user
     *
     * @api
     * @return string.
     */

    public function getBoxxWidgetInstances(){
        $returnData = array();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        $client = $this->scopeConfig->getValue(self::XML_PATH_CLIENT, $storeScope);

        $returnData = array();
        $widgetCollection = $this->_collectionFactory->create();
        $widgetCollection->addFieldToSelect("*")->addFieldToFilter('instance_type',static::BLOCK_PRODUCT_WIDGET);

        $widgetCollection->getSelect()->joinLeft(
            array('wip' => 'widget_instance_page'),
            "main_table.instance_id = wip.instance_id","wip.page_group"
        );

        foreach ($widgetCollection as $widget) {
            //print_r($widget->getData());
            $parameters = $widget->getWidgetParameters();
            //$data = array();
            $data["widgetInstance"] = $parameters["display_type"];
            $data["page_name"] = $widget->getPageGroup();
            $data["widgetTitle"] = $parameters["title"];
            $data["client"] = $client;
            $data["status"] = $parameters["widget_status_boxx"];
            $returnData[] = $data;
        
        }

        if(count($returnData)){
            return json_encode($returnData);            
        }else{
            return json_encode(array('Message' => "No data found"));
        }

    }
}

