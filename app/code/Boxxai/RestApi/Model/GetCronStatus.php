<?php
namespace Boxxai\RestApi\Model;
use Boxxai\RestApi\Api\GetCronStatusInterface;

class GetCronStatus implements GetCronStatusInterface
{
    /** @var \Magento\Cron\Model\Schedule*/
    protected $_cronCollectionFactory;

    protected $_date;

    public function __construct(
        \Magento\Cron\Model\ScheduleFactory $cronCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->_cronCollectionFactory = $cronCollectionFactory;
        $this->_date = $date;
    }

    /**
     * Returns Cron status
     *
     * @api
     * @return bool.
     */
    public function getCronStatus(){
        $cronCollection  = $this->_cronCollectionFactory->create()->getCollection();
        $cronCollection->addFieldToFilter('job_code',array("in"=>array('boxxai_cron_prod_sync','boxxai_cron_order_sync')));
        $now = $this->_date->timestamp(time());
        $enddate = date('Y-m-d', $now);
        $startdate = date('Y-m-d',strtotime("-15 days", strtotime($enddate)));
        $cronCollection->addFieldToFilter('main_table.scheduled_at', array('from' => $startdate.' 00:00:00', 'to' => $enddate.' 23:59:59'));
        if($cronCollection->count()){
            return true;          
        }else{
            return false;
        }
    }
}

