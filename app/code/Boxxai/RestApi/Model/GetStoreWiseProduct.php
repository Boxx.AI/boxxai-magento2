<?php
namespace Boxxai\RestApi\Model;
use Boxxai\RestApi\Api\GetStoreWiseProductInterface;

class GetStoreWiseProduct implements GetStoreWiseProductInterface
{

    /**
     *@var Magento\Store\Model\StoreRepository 
     */

    protected $_storeManager;

   /** 
    *@var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
    */
    protected $_collectionFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
    ) {
        $this->_storeManager = $storeManager;
        $this->_collectionFactory = $collectionFactory;
    }


    public function getStoreWiseProduct(){
        $stores = $this->_storeManager->getStores($withDefault = false);
        $store_wise_products =array();
        foreach ($stores as $store) {
            $collection = $this->_collectionFactory->create();
            $collection->addStoreFilter($store->getStoreId());
            $store_wise_products[$store->getStoreId()] = $collection->count();
        }

        return json_encode($store_wise_products);
    }
}

