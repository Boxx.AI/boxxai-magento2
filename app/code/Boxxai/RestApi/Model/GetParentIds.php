<?php
namespace Boxxai\RestApi\Model;
use Boxxai\RestApi\Api\GetParentIdsInterface;

class GetParentIds implements GetParentIdsInterface
{
    protected $_configurableProduct;

    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory */
    protected $_collectionFactory;

    protected $_storeManager;


    public function __construct(
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProduct,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collection,
        \Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_configurableProduct = $configurableProduct;
        $this->_collectionFactory = $collection;
        $this->_storeManager = $storeManager;
    }

    /**
     * Returns greeting message to user
     *
     * @api
     * @param mixed $products Users name.
     * @return mixed Greeting message with products name.
     */

    public function getParentIds($products){

        $product_ids = array();
        $productsCollection = $this->_collectionFactory
                                   ->create()
                                   ->addAttributeToSelect('image','type_id','url_key','parent_id')
                                   ->addAttributeToFilter('entity_id',['in'=> $products]);

        
        foreach ($productsCollection as $product) {

            $producturl = $product->getProductUrl();
            $imageurl = $this->_storeManager->getStore()->getBaseUrl() . 'catalog/product' . $product['image'];

            $parent_id = $this->_configurableProduct->getParentIdsByChild($product['entity_id']);
            if($parent_id){
                $product_ids[$product['entity_id']] = array('pid' => $parent_id[0],'producturl' => $producturl,'imageurl' => $imageurl);
            }else{
                $product_ids[$product['entity_id']] = array('pid' => null,'producturl' => $producturl,'imageurl' => $imageurl);
            }

        }
        
        if(count($product_ids)){
            echo json_encode($product_ids);
        }else{
            echo json_encode(array('Message' => "No data found"));
        }
    }
}

