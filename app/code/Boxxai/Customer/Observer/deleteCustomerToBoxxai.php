<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Customer\Observer;


class deleteCustomerToBoxxai implements \Magento\Framework\Event\ObserverInterface
{
	const BOXXAI_DATA_TYPE = 'customer';

    /**
    * @var \Boxxai\Core\Api\DeleteSpecificApi 
    */
    protected $_deleteSpecificApi;

    /**
    * @var \Boxxai\Core\Model\DeleteSpecific 
    */
    protected $_deleteRequest;

    /**
    * @var \Boxxai\Core\Helper\Data
    */
    protected $_apiConfig;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /**
    *
    * @param \Boxxai\Core\Helper\Data $_apiConfig
    * @param \Boxxai\Core\Api\DeleteSpecificApi $_deleteSpecificApi
    * @param \Boxxai\Core\Model\DeleteSpecific $_deleteRequest
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Boxxai\Core\Helper\Data $_apiConfig,
        \Boxxai\Core\Api\DeleteSpecificApi $_deleteSpecificApi,
        \Boxxai\Core\Model\DeleteSpecific $_deleteRequest,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_deleteSpecificApi = $_deleteSpecificApi;
        $this->_deleteRequest = $_deleteRequest;
        $this->_apiConfig = $_apiConfig;
        $this->logger = $loggerInterface;
    }


    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        //no interaction if campaign id is incorrect
        if($this->_apiConfig->getCampaignId()=="")
        {
            return;
        }
        $customer = $observer->getEvent()->getCustomer();

        $data = array($customer->getId());
             
        $this->_deleteRequest->setClientId($this->_apiConfig->getClientId());
        $this->_deleteRequest->setAccessToken($this->_apiConfig->getAccessToken());
        $this->_deleteRequest->setData($data);
        $this->_deleteRequest->setDataType(self::BOXXAI_DATA_TYPE);
        
        try {
            $result = $this->_deleteSpecificApi->apiDataDeleteDelete($this->_deleteRequest);
            // print_r($result);
        } catch (\Exception $e) {
            //echo 'Exception when calling UploadDataApi->apiDataDeleteDelete: ', $e->getMessage(), PHP_EOL;
            return;
        }
    }
	
}