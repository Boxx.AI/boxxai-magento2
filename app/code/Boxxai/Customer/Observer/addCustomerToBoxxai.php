<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Customer\Observer;


class addCustomerToBoxxai implements \Magento\Framework\Event\ObserverInterface{

    /**
    * @var \Boxxai\Core\Api\UploadDataApi
    */
    protected $_uploadDataApi;

    /**
    * @var \Boxxai\Core\Model\UploadCustomer
    */
    protected $_uploadCustomer;

    /**
    * @var \Boxxai\Core\Model\UploadCustomerRequest 
    */
    protected $_uploadCustomerRequest;

    /**
    * @var \Boxxai\Core\Helper\Data
    */
    protected $_apiConfig;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;
    protected $customers;

    /**
    *
    * @param \Boxxai\Core\Helper\Data $_apiConfig
    * @param \Boxxai\Core\Api\UploadDataApi $_uploadDataApi
    * @param \Boxxai\Core\Model\UploadCustomer $_uploadCustomer
    * @param \Boxxai\Core\Model\UploadCustomerRequest $_uploadCustomerRequest
    *
    */
    public function __construct(
        \Boxxai\Core\Helper\Data $_apiConfig,
        \Boxxai\Core\Api\UploadDataApi $_uploadDataApi,
        \Boxxai\Core\Model\UploadCustomer $_uploadCustomer,
        \Boxxai\Core\Model\UploadCustomerRequest $_uploadCustomerRequest,
        \Magento\Customer\Model\Customer $customers,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_uploadDataApi = $_uploadDataApi;
        $this->_uploadCustomer = $_uploadCustomer;
        $this->_uploadCustomerRequest = $_uploadCustomerRequest;
        $this->_apiConfig = $_apiConfig;
        $this->_customer=$customers;
        $this->logger = $loggerInterface;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
         if($this->_apiConfig->getCampaignId()=="")
        {
            return;
        }
		
        $obsvCustomer = $observer->getEvent()->getCustomer();
        $customer=$this->_customer->load($obsvCustomer->getId());
        /*$customer->setData('boxx_customer_flag', '1')->getResource()->saveAttribute($customer, 'boxx_customer_flag');*/
    }
}