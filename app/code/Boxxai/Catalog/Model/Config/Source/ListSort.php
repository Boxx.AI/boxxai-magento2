<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Catalog Product List Sortable allowed sortable attributes source
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Boxxai\Catalog\Model\Config\Source;


class ListSort extends \Magento\Catalog\Model\Config\Source\ListSort
{
    protected $_moduleManager;

    protected $_scopeConfig;
    public function __construct(
    \Magento\Framework\Module\Manager $moduleManager,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Catalog\Model\Config $catalogConfig
    ){ 
        $this->_moduleManager = $moduleManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_catalogConfig = $catalogConfig;
        parent::__construct($catalogConfig);
    }

    /**
     * Retrieve option values array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $configPath = "boxxai_section/boxxai_options_group/plp_show";
        $value =  $this->_scopeConfig->getValue( $configPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

        $options = [];
        if ($this->_moduleManager->isOutputEnabled('Boxxai_Catalog') && $value == 1 ) {
            $options[] = ['label' => __('Magic'), 'value' => 'ai_magic'];
        }
        $options[] = ['label' => __('Position'), 'value' => 'position'];
        foreach ($this->_getCatalogConfig()->getAttributesUsedForSortBy() as $attribute) {
            $options[] = ['label' => __($attribute['frontend_label']), 'value' => $attribute['attribute_code']];
        }
        return $options;
    }

}
