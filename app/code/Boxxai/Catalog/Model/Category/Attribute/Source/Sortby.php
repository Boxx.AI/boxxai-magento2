<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Boxxai\Catalog\Model\Category\Attribute\Source;

/**
 * Catalog Category *_sort_by Attributes Source Model
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Sortby extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Catalog config
     *
     * @var \Magento\Catalog\Model\Config
     */
    protected $_catalogConfig;

    protected $_moduleManager;

    protected $_scopeConfig;
    /**
     * Construct
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     */
    public function __construct(\Magento\Catalog\Model\Config $catalogConfig,
        \Magento\Framework\Module\Manager $moduleManager,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->_moduleManager = $moduleManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_catalogConfig = $catalogConfig;
    }

    /**
     * Retrieve Catalog Config Singleton
     *
     * @return \Magento\Catalog\Model\Config
     */
    protected function _getCatalogConfig()
    {
        return $this->_catalogConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $configPath = "boxxai_section/boxxai_options_group/plp_show";
            $value =  $this->_scopeConfig->getValue( $configPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );

            if ($this->_moduleManager->isOutputEnabled('Boxxai_Catalog') && $value == 1 ) {
                $this->_options = [['label' => __('Magic'), 'value' => 'ai_magic'],['label' => __('Position'), 'value' => 'position']];
            }else{
                $this->_options = [['label' => __('Position'), 'value' => 'position']];
            }
            
            foreach ($this->_getCatalogConfig()->getAttributesUsedForSortBy() as $attribute) {
                $this->_options[] = [
                    'label' => __($attribute['frontend_label']),
                    'value' => $attribute['attribute_code'],
                ];
            }
        }
        return $this->_options;
    }
}
