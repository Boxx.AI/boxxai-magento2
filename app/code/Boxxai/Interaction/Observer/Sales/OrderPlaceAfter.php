<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Interaction\Observer\Sales;

class OrderPlaceAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /**
    * Construct
    *
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
		\Psr\Log\LoggerInterface $loggerInterface		
    ) {
        $this->_session = $sessionManager;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) { 	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_apiConfig = $objectManager->create('Boxxai\Core\Helper\Data');
		if($_apiConfig->getCampaignId()=="")
		{
			return;
		}
		$orderIds = $observer->getEvent()->getOrderIds();
		if($orderIds)
		{
			$order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderIds[0]);
			//$order->setData('boxx_order_flag', '1')->getResource()->saveAttribute($order, 'boxx_order_flag');  
			
			if($this->_session->getBoxxaiSessionData()){
				$sessiondata = $this->_session->getBoxxaiSessionData();
			}
			if($this->_session->getBoxxaiSessionDataOrderPlaced()){
				$sessiondataOrderPlaced = $this->_session->getBoxxaiSessionDataOrderPlaced();
			}
			
			$productIds = [];
			$productIds = $sessiondataOrderPlaced['boxxai_session']['CheckoutItemsOrderPlaced'];
			

			$productData = [
				'product_ids' => $productIds,
				'type'=>'OrderPlaced',
				'transactionid'=>$orderIds[0]
			];
			
			$sessiondata['boxxai_session']['OrderPlaced'] = $productData; 
			
			$this->_session->setBoxxaiSessionData($sessiondata);
			$this->_session->unsBoxxaiSessionDataOrderPlaced();
		}
		
    }
}
