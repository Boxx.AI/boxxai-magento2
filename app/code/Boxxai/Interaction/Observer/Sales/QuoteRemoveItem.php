<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Interaction\Observer\Sales;

class QuoteRemoveItem implements \Magento\Framework\Event\ObserverInterface
{
    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /**
    * Construct
    *
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_session = $sessionManager;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_apiConfig = $objectManager->create('Boxxai\Core\Helper\Data');
		if($_apiConfig->getCampaignId()=="")
		{
			return;
		}
        $quoteItem = $observer->getQuoteItem();
		if($quoteItem)
		{
			//Check if item is configurable
			if($quoteItem->getProduct()->getTypeId() == "configurable")
			{
				// Get Child Product Id
				$productIds[]=$quoteItem->getOptionByCode('simple_product')->getProduct()->getId();
			}
			else
			{
				$productIds[] = $quoteItem->getProductId();
			}    

			$sessiondata = array();

			if($this->_session->getBoxxaiSessionData()){
				$sessiondata = $this->_session->getBoxxaiSessionData();
			}

			$productData = [
				'product_ids' => $productIds,
				'type'=>'RemoveFromCart'
			];

			$sessiondata['boxxai_session']['RemoveFromCart_'.uniqid()] = $productData; 

			$this->_session->setBoxxaiSessionData($sessiondata);
		}
		
    }
}