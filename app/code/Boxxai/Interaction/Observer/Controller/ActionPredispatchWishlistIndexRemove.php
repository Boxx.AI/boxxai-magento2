<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Interaction\Observer\Controller;

class ActionPredispatchWishlistIndexRemove implements \Magento\Framework\Event\ObserverInterface
{
    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /** @var Magento\Wishlist\Model\ItemFactory */
    protected $_itemFactory;

    protected $_request;

    /**
    * Construct
    *
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    * @param \Magento\Wishlist\Model\ItemFactory $itemFactory
    * @param \Magento\Framework\App\Request\Http $request
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Magento\Wishlist\Model\ItemFactory $itemFactory,
        \Magento\Framework\App\Request\Http $request,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_session = $sessionManager;
        $this->_itemFactory = $itemFactory;
        $this->_request = $request;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_apiConfig = $objectManager->create('Boxxai\Core\Helper\Data');
		if($_apiConfig->getCampaignId()=="")
		{
			return;
		}
        $itemId= (int)$this->_request->getParam('item');
        $wishlistItem = $this->_itemFactory->create()->load($itemId);
		if($wishlistItem)
		{
			$_product = $wishlistItem->getProduct();
			$sessiondata = array();

			if($this->_session->getBoxxaiSessionData()){
				$sessiondata = $this->_session->getBoxxaiSessionData();
			}

			$productData = [
				'product_ids' => array($_product->getId()),
				'type'=>'RemoveFromWishlist'
			];

			$sessiondata['boxxai_session']['RemoveFromWishlist_'.uniqid()] = $productData; 

			$this->_session->setBoxxaiSessionData($sessiondata);
		}
        
    }
}