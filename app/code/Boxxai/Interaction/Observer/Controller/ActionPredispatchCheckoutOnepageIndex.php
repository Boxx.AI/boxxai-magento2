<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Interaction\Observer\Controller;

class ActionPredispatchCheckoutOnepageIndex implements \Magento\Framework\Event\ObserverInterface
{
    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

     /** @var CheckoutSession */
    protected $checkoutSession;

    /**
    * Construct
    *
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    * @param \Magento\Checkout\Model\Session $checkoutSession
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_session = $sessionManager;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) { 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_apiConfig = $objectManager->create('Boxxai\Core\Helper\Data');
		if($_apiConfig->getCampaignId()=="")
		{
			return;
		}
        $quote = $this->checkoutSession->getQuote(); 
		if($quote)
		{
			$productIds = array();
			foreach ($quote->getAllVisibleItems() as $item) {
				if($item->getProductType() == "configurable")
				{
					$productIds[] = $item->getOptionByCode('simple_product')->getProduct()->getId();
				}
				else
				{
					$productIds[] = $item->getProduct()->getId();
				}            
			}

			$sessiondata = array();
			$sessiondataOrderPlaced = array();

			if($this->_session->getBoxxaiSessionData()){
				$sessiondata = $this->_session->getBoxxaiSessionData();
			}

			$productData = [
				'product_ids' => $productIds,
				'type'=>'CheckoutItems'
			];
			
			$productDataOrderPlaced = $productIds;

			$sessiondata['boxxai_session']['CheckoutItems'] = $productData; 
			$sessiondataOrderPlaced['boxxai_session']['CheckoutItemsOrderPlaced'] = $productDataOrderPlaced; 

			$this->_session->setBoxxaiSessionData($sessiondata);
			$this->_session->setBoxxaiSessionDataOrderPlaced($sessiondataOrderPlaced);
		}
    }
}
