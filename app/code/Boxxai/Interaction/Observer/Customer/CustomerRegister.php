<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Interaction\Observer\Customer;

class CustomerRegister implements \Magento\Framework\Event\ObserverInterface
{

    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /**
    * Construct
    *
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_session = $sessionManager;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_apiConfig = $objectManager->create('Boxxai\Core\Helper\Data');
		if($_apiConfig->getCampaignId()=="")
		{
			return;
		}
		$obsvCustomer = $observer->getEvent()->getCustomer();
		if($obsvCustomer)
		{
			$sessiondata = array();
			$productID='site';

			if($this->_session->getBoxxaiSessionData()){
				$sessiondata = $this->_session->getBoxxaiSessionData();
			}
		
			$customerData = [
				'custid' => (($obsvCustomer->getId())?$obsvCustomer->getId():""),
				'product_ids' => array($productID),
				'type'=>'sign_up',
				'firstname'=>(($obsvCustomer->getFirstname())?$obsvCustomer->getFirstname():""),
				'lastname'=>(($obsvCustomer->getLastname())?$obsvCustomer->getLastname():""),
				'email'=>""//(($obsvCustomer->getEmail())?$obsvCustomer->getEmail():"")
			];

			$sessiondata['boxxai_session']['CustomerRegister'] = $customerData; 

			$this->_session->setBoxxaiSessionData($sessiondata);
		}
        
    }
}
