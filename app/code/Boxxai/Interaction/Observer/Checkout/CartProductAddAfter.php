<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Interaction\Observer\Checkout;

class CartProductAddAfter implements \Magento\Framework\Event\ObserverInterface
{

    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /**
    * Construct
    *
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_session = $sessionManager;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_apiConfig = $objectManager->create('Boxxai\Core\Helper\Data');
		if($_apiConfig->getCampaignId()=="")
		{
			return;
		}
        $eventType  = $observer->getEvent()->getName();
        $item = $observer->getQuoteItem();
		if($item)
		{
			$_product = $item->getProduct();

			$sessiondata = array();
			$productID=$_product->getId();
			if($item->getProductType() == "configurable")
			{
				// Get Child Product Id
				$productID=$item->getOptionByCode('simple_product')->getProduct()->getId();

			}

			if($this->_session->getBoxxaiSessionData()){
				$sessiondata = $this->_session->getBoxxaiSessionData();
			}
		
			$productData = [
				'product_ids' => array($productID),
				'type'=>'AddToCart'
			];

			$sessiondata['boxxai_session']['AddToCart_'.uniqid()] = $productData; 

			$this->_session->setBoxxaiSessionData($sessiondata);
		}
        
    }
}
