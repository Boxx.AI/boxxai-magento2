<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Interaction\Observer\Catalog;

class ProductCompareAddProduct implements \Magento\Framework\Event\ObserverInterface
{

    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;

    /**
    * Construct
    *
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    * @param \Psr\Log\LoggerInterface $loggerInterface
    *
    */
    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->_session = $sessionManager;
        $this->logger = $loggerInterface;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_apiConfig = $objectManager->create('Boxxai\Core\Helper\Data');
		if($_apiConfig->getCampaignId()=="")
		{
			return;
		}
        $eventType  = $observer->getEvent()->getName();
        $_product = $observer->getEvent()->getProduct();
		if($_product)
		{
			 $sessiondata = array();

			if($this->_session->getBoxxaiSessionData()){
				$sessiondata = $this->_session->getBoxxaiSessionData();
			}

			$productData = [
				'product_ids' => array($_product->getId()),
				'type'=>'AddToCompare'
			];

			$sessiondata['boxxai_session']['AddToCompare_'.uniqid()] = $productData; 

			$this->_session->setBoxxaiSessionData($sessiondata); 
		}
              
    }
}
