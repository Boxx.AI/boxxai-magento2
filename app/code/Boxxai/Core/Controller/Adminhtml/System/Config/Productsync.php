<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */



namespace Boxxai\Core\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Boxxai\Core\Helper\Data;

class Productsync extends Action
{

    protected $resultJsonFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Data $helper
    )
    {
        parent::__construct($context);
		$this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        
    }

    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            $stores = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStores($withDefault = false);
            $messageManager = $this->_objectManager->get('Magento\Framework\Message\ManagerInterface');
            $syncData = $this->_getSync();
            foreach ($stores as $store) {
                $syncData->markAllProductsForUpdate($store);
            }
            $this->messageManager->addSuccess(__("Product Sync scheduled to be run on the next cron"));
        } catch (\Exception $e) {
            $this->messageManager->addError(__("Unable to resync product"));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
    }

    /**
     * Return product relation singleton
     *
     * @return \MageWorx\AlsoBought\Model\Relation
     */

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Boxxai_Core::config');
    }

    protected function _getSync()
    {
        return $this->_objectManager->create('Boxxai\Cron\Cron\Syncdata');
    }

}
