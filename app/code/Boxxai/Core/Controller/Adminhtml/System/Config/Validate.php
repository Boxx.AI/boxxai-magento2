<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */



namespace Boxxai\Core\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Boxxai\Core\Helper\Data;

class Validate extends Action
{

    protected $resultJsonFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Data $helper
    )
    {
        parent::__construct($context);
		$this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        
    }

    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
           $post = $this->getRequest()->getPost();
           $client_id = $post['client_id'];
           $access_token = $post['access_token'];
           $optimise_for = $post['optimise_for'];
           $ch = curl_init("http://app.boxx.ai/client/validate/?client_id=".$client_id."&access_token=".$access_token."&primary_transaction_type=".$optimise_for."");
           $resultArray =  curl_exec($ch);
           curl_close($ch);
        } catch (\Exception $e) {
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
    }

    /**
     * Return product relation singleton
     *
     * @return \MageWorx\AlsoBought\Model\Relation
     */

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Boxxai_Core::config');
    }
}
