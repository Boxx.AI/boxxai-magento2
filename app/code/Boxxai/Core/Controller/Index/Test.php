<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Core\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Test extends Action
{
    protected $_session;
	protected $_productCollectionFactory;
    
    public function __construct(
		\Boxxai\Product\Model\ResourceModel\Product $productCollectionFactory,
        Context $context		
    ) {
        parent::__construct($context);  
		$this->_productCollectionFactory = $productCollectionFactory;		
    }
	
    public function execute()
    {
		
		/* $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_orderCollectionFactory = $objectManager->get('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
		$orderFlagsToUpdate=$_orderCollectionFactory->create();
        $orderFlagsToUpdate->addAttributeToFilter('boxx_order_flag',[['neq'=>'1'],['null'=>true],['eq' => 'NO FIELD'],['eq' => '']]);
		echo $orderFlagsToUpdate->getSize();
		die;
		 
		 $storeManager = $objectManager->get('Boxxai\Cron\Cron\Syncdata');
		 $storeManager->orderFlagUpdate(); */
    }
	protected function _isAllowed(){
		return true;
    }

}
