<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Core\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Session\SessionManager as TokenSessionManager;

class Settoken extends Action
{
    protected $_session;
    
    public function __construct(
        Context $context,
		TokenSessionManager $session
    ) {
        parent::__construct($context);
		$this->_session = $session;        
    }
	
    public function execute()
    {           
        $this->_session->setBoxxTokenId($this->getRequest()->getParam('boxx_token_id'));
    }
	protected function _isAllowed(){
		return true;
    }

}
