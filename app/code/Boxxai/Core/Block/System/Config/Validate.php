<?php
/**
 * Copyright © 2016 Boxx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Boxxai\Core\Block\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Validate extends Field
{
    /**
     * @var string
     */
    protected $_template = 'Boxxai_Core::system/config/validate.phtml';

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Remove scope label
     *
     * @param  AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for collect button
     *
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('boxxai_core/system_config/validate');
    }

    /**
     * Generate collect button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'validate_button',
                'label' => __('Validate'),
            ]
        );

        return $button->toHtml();
    }
    public function setCampaignCurrentID()
    {
        //fetch apiconfig details
        $boxxapiconfig = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Boxxai\Core\Helper\Data');        
        $boxxsessionManager = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Framework\Session\SessionManager');
        $boxxsessionManager->setCampaignCurrentID($boxxapiconfig->getCampaignId());
    }
}