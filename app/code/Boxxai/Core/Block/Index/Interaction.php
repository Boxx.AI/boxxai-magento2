<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Core\Block\Index;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Boxxai\Core\Helper\Data as BoxxHelperData;

class Interaction extends Template
{
  protected $urlBuider;

  protected $_apiConfig;

  protected $_session;

  protected $customerSession;


  public function __construct(
    Context $context,
    BoxxHelperData $_apiConfig,
    \Magento\Customer\Model\Session $customerSession,
    array $data = []
  ){
    parent::__construct($context, $data);
	$this->urlBuilder = $context->getUrlBuilder();    
    $this->_session = $context->getSession();
    $this->_apiConfig = $_apiConfig;
    $this->customerSession = $customerSession;
    
  }

  public function getBoxxInteractionContent(){
    $boxxaiContent = new \Magento\Framework\DataObject();
    if($this->_session->getBoxxaiSessionData()){
      $boxxaiContent = $this->_session->getBoxxaiSessionData();
    }
	
	$boxxjsoncontent=json_encode($boxxaiContent);
    return (string)$boxxjsoncontent;
  }

  public function getCurrentCustomerId(){
    $boxxcustomerId = "";
    if($this->customerSession->isLoggedIn()) {
        $boxxcustomerId = $this->customerSession->getCustomer()->getId();
    }
    return $boxxcustomerId;
  }

  public function unsetBoxxInteractionContent(){
    if($this->_session->getBoxxaiSessionData()){
      $this->_session->unsBoxxaiSessionData();
    }
  }
}
