<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */

namespace Boxxai\Core\Block\Index;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Boxxai\Core\Helper\Data as BoxxHelperData;

class Header extends Template
{
  protected $urlBuider;

  protected $_apiConfig;

  protected $_session;

  protected $customerSession;

  public function __construct(
    Context $context,
    BoxxHelperData $_apiConfig,
    \Magento\Customer\Model\Session $customerSession,
    array $data = []
  ){
    parent::__construct($context, $data);
	$this->urlBuilder = $context->getUrlBuilder();    
    $this->_session = $context->getSession();
	$this->_apiConfig = $_apiConfig;
    $this->customerSession = $customerSession;
    
  }

  public function getClientJsUrl(){
    return "https://js.boxx.ai/?client_id=".$this->_apiConfig->getClientId();
  }

  public function getBoxxTokenConsumerUrl (){
    return $this->urlBuilder->getUrl('boxxaicore/index/settoken', ['_secure' => true], $paramsHere = array('tokenid'=>$this->_session->getSessionId()));
  }

  public function getCurrentCustomerId(){
    $boxxcustomerId = "";
    if($this->customerSession->isLoggedIn()) {
        $boxxcustomerId = $this->customerSession->getCustomer()->getId();
    }
    return $boxxcustomerId;
  }
}
