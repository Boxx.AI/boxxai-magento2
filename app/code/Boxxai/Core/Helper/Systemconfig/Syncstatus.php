<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Core\Helper\Systemconfig;

use Magento\Framework\Option\ArrayInterface;
 
class Syncstatus implements ArrayInterface
{    
    const PAUSED     = 'paused';
    const ACTIVE  = 'active';
 
    /**
     * Get positions of lastest news block
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            self::PAUSED => __('Paused'),
            self::ACTIVE => __('Active')
        ];
    }
}
