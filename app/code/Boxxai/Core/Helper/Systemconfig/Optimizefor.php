<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Core\Helper\Systemconfig;

use Magento\Framework\Option\ArrayInterface;
 
class Optimizefor implements ArrayInterface
{
    const VIEWS      = 'ProductView';
    const PURCHASE     = 'OrderPlaced';
    const REVENUE  = 'revenue';
 
    /**
     * Get positions of lastest news block
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            self::VIEWS => __('Engagement'),
            self::PURCHASE => __('Purchase'),
            self::REVENUE => __('Revenue')
        ];
    }
}
