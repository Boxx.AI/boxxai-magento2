<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\StoreManagerInterface;


Class Data extends AbstractHelper{

	/**
	* constant defined
	*/
	const BOXXAI_CLIENT_ID 		= 'boxxai_section/boxxai_group/client_id';

   const BOXXAI_ACCESS_TOKEN  = 'boxxai_section/boxxai_group/access_token';

   const BOXXAI_CAMPAIGN_ID  = 'boxxai_section/boxxai_group/campaign_id';

   const BOXXAI_PRODUCT_SYNC  = 'boxxai_section/boxxai_group/product_sync';

   const BOXXAI_CUSTOMER_SYNC  = 'boxxai_section/boxxai_group/customer_sync';

	const BOXXAI_ORDER_SYNC 	= 'boxxai_section/boxxai_group/order_sync';
   	
	const BOXXAI_PRODUCT_PLP_SHOW 	= 'boxxai_section/boxxai_options_group/plp_show';
	
	const BOXXAI_SHOW_BOXX_NAME 	= 'boxxai_section/boxxai_options_group/sort_by_boxx_name';

   /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
   protected $scopeConfig;

   /**
   * @var \Magento\Store\Model\ScopeInterface
   */
   protected $storeScope;

   /**
   * @var \Magento\Store\Model\StoreManagerInterface
   */
   protected $storeManagerInterface;

   /**
   *  @var \Magento\Framework\App\Config\Storage\WriterInterface
   */
   protected $configWriter;


   /**
   * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
   * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
   * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
   */
   public function __construct(
   	ScopeConfigInterface $scopeConfig,
    WriterInterface $configWriter,
   	StoreManagerInterface $storeManagerInterface
   )
   {
      $this->scopeConfig = $scopeConfig;
      $this->storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;      
      $this->configWriter = $configWriter;
      $this->storeManagerInterface = $storeManagerInterface;
   }

   /**
   * get boxxai client id
   * @return string
   */

   public function getClientId(){
   		return $this->scopeConfig->getValue(self::BOXXAI_CLIENT_ID, $this->storeScope);
   }

   /**
   * get accesstoken
   * @return string
   */
   public function getAccessToken(){
   		return $this->scopeConfig->getValue(self::BOXXAI_ACCESS_TOKEN, $this->storeScope);
   }


   /**
   * get campaignid
   * @return string
   */
   public function getCampaignId(){
         return $this->scopeConfig->getValue(self::BOXXAI_CAMPAIGN_ID, $this->storeScope);
   }

   /**
   * get product sync
   * @return boolean
   */
   public function isProductSync(){
         return $this->scopeConfig->getValue(self::BOXXAI_PRODUCT_SYNC, $this->storeScope);
   }

   /**
   * get customer sync
   * @return boolean
   */
   public function isCustomerSync(){
         return $this->scopeConfig->getValue(self::BOXXAI_CUSTOMER_SYNC, $this->storeScope);
   }

   /**
   * get order sync
   * @return boolean
   */
   public function isOrderSync(){
         return $this->scopeConfig->getValue(self::BOXXAI_ORDER_SYNC, $this->storeScope);
   }
   
   /**
   * get plpStatusShow
   * @return string
   */
   public function plpStatusShow(){
         return (boolean)$this->scopeConfig->getValue(self::BOXXAI_PRODUCT_PLP_SHOW, $this->storeScope);
   }

   /**
   * get plpMagicName
   * @return string
   */
   public function plpMagicName(){
         return $this->scopeConfig->getValue(self::BOXXAI_SHOW_BOXX_NAME, $this->storeScope);
   }
   

   public function updateConfig($path,$value){      
        $this->configWriter->save($path, $value);
   }

   /**
   * Get Unique transaction/intractionKey
   * @return string
   *
   */
   public function getTransactionKey(){
   	return md5(uniqid(rand(), true));
   }

   /**
   * Get Media Url
   * @return string
   *
   */
   public function getMediaUrl(){
   		$store = $this->storeManagerInterface->getStore();
   		return $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
   }

	 public function getValidateClient($optimise_type){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "http://app.boxx.ai/client/validate/?client_id=".$this->getClientId().	"&access_token=".$this->getAccessToken()."&primary_transaction_type=".$optimise_type,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);

		return array('response'=>$response,'error'=>$err);
		curl_close($curl);
	 }
}
