<?php

namespace Boxxai\Core\Model\Config\Developer;

class Cronexpressionsproduct implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Get options.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '*/2 * * * *', 'label' => 'Every 2 Minutes'],
            ['value' => '*/5 * * * *', 'label' => 'Every 5 Minutes'],
            ['value' => '*/10 * * * *', 'label' => 'Every 10 Minutes'],
            ['value' => '*/15 * * * *', 'label' => 'Every 15 Minutes'],
            ['value' => '0 * * * *', 'label' => 'Hourly'],
            ['value' => '0 */3 * * *', 'label' => 'Every 3 Hours'],
            ['value' => '0 */6 * * *', 'label' => 'Every 6 Hours'],
        ];
    }
}
