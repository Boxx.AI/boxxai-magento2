<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Core\Observer\Systemconfig;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager as BoxxSettingsSessionManager;
use Boxxai\Core\Helper\Data as BoxxHelperData;


class Boxxsettingsonsave implements ObserverInterface
{

    protected $_apiConfig;

    
    /**
    * @var \Magento\Framework\Session\SessionManager
    */
    protected $_session;

    /**
    *
    * @param \Boxxai\Core\Helper\Data $_apiConfig    
    * @param \Magento\Framework\Session\SessionManager $sessionManager
    *
    */
    public function __construct(
        BoxxHelperData $_apiConfig,
        BoxxSettingsSessionManager $sessionManager
    ) {
        $this->_apiConfig = $_apiConfig;
        $this->_session = $sessionManager;
    }


    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        // if campaign id is blank disable everything
		if($this->_apiConfig->getCampaignId()=="")
        {
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_PRODUCT_SYNC,"paused");
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_CUSTOMER_SYNC,"paused");
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_ORDER_SYNC,"paused");
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_PRODUCT_PLP_SHOW,0);
        }

        // if customer changes api settings enable sync
        if($this->_apiConfig->getCampaignId() != $this->_session->getCampaignCurrentID())
        {
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_PRODUCT_SYNC,"paused");
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_CUSTOMER_SYNC,"paused");
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_ORDER_SYNC,"paused");
          $this->_apiConfig->updateConfig($this->_apiConfig::BOXXAI_PRODUCT_PLP_SHOW,0);
        }
		
		if(($this->_apiConfig->isProductSync()=="active") && ($this->_apiConfig->isProductSync()))
		{
			$this->_apiConfig->updateConfig("catalog/frontend/default_sort_by","relevance_ai");
		}
    }
}
