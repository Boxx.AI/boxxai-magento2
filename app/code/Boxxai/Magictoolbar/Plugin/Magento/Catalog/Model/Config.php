<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Magictoolbar\Plugin\Magento\Catalog\Model;

use Magento\Store\Model\StoreManagerInterface;

class Config
{

	/**
	 * Adding custom options and changing labels
	 *
	 * @param \Magento\Catalog\Model\Config $catalogConfig
	 * @param [] $options
	 * @return []
	 */

    public function afterGetAttributeUsedForSortByArray(
        \Magento\Catalog\Model\Config $subject,
        $options
    ) {
    	$apiConfig = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Boxxai\Core\Helper\Data');

    	// Set Sort By Filter Name
    	if($apiConfig->plpMagicName()=="")
    	{
    		//New sorting options
	    	$customOption['ai_magic'] = __(' Magic');
    	}    
    	else
    	{
    		$customOption['ai_magic'] = __($apiConfig->plpMagicName());    		
    	}

    	// Enable or Disable Sort By Option
    	if($apiConfig->plpStatusShow())
    	{
    		//Merge default sorting options with custom options
	    	$options = array_merge($customOption, $options);	    	
    	}
    	return $options;
    }
}
