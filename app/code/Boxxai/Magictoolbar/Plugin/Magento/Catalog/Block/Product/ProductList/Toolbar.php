<?php

/**
 * Boxx Ai Plugin
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @user     Boxx Team
 */


namespace Boxxai\Magictoolbar\Plugin\Magento\Catalog\Block\Product\ProductList;

class Toolbar
{
  /**
   * @var \Boxxai\Core\Helper\Data
   */
    protected $_apiConfig;

    /**
     * @var \Boxxai\Magic\Api\RecommendationApi
     */
    protected $_recommendationApi;

    /**
     * @var \Boxxai\Magic\Model\RecommendationRequestData
     */
    protected $_recommendationRequestData;

    /**
     * @var \Boxxai\Magic\Model\RecommendationRequestDataQuery
     */
    protected $_recommendationRequestDataQuery;

    /**
     * @var \Magento\Framework\Session\SessionManager
     */
    protected $_session;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
     protected $request;
     protected $_count;

    /**
     * @param \Boxxai\Magic\Api\RecommendationApi $_recommendationApi
     * @param \Boxxai\Magic\Model\RecommendationRequestData $_recommendationRequestData
     * @param \Boxxai\Magic\Model\RecommendationRequestDataQuery $_recommendationRequestDataQuery
     * @param \Magento\Framework\Session\SessionManager $sessionManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Boxxai\Core\Helper\Data $_apiConfig
     */


    public function __construct(
        \Boxxai\Magic\Api\RecommendationApi $_recommendationApi,
        \Boxxai\Magic\Model\RecommendationRequestData $_recommendationRequestData,
        \Boxxai\Magic\Model\RecommendationRequestDataQuery $_recommendationRequestDataQuery,
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Magento\Customer\Model\Session $customerSession,
        \Boxxai\Core\Helper\Data $_apiConfig,
	\Magento\Framework\App\Request\Http $request
    ){
        $this->_apiConfig = $_apiConfig;
        $this->_recommendationApi = $_recommendationApi;
        $this->_recommendationRequestData = $_recommendationRequestData;
        $this->_recommendationRequestDataQuery = $_recommendationRequestDataQuery;
        $this->_session = $sessionManager;
        $this->customerSession = $customerSession;
		$this->request = $request;
		$this->_count =1;
    }

    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        \Closure $proceed,
        $collection
    ) {
		if($this->_count==1){
		try
		{
			$this->_count=2;
			$currentOrder = $subject->getCurrentOrder();
			$result = $proceed($collection);
			
			if($this->_apiConfig->getCampaignId() == ""){
			  return $result;
			}

			if($this->_apiConfig->isProductSync() != "active"){
			  return $result;
			}        
			
			$objectManagerCms = \Magento\Framework\App\ObjectManager::getInstance();
			$pageIdentifier = $objectManagerCms->get('\Magento\Framework\App\Request\Http');
			
			if($pageIdentifier->getFullActionName()!="catalog_category_view")
			{
				return $result;
			}
		}catch (\Exception $e) {
			return $result;
		}catch (\Error $e) {
			return $result;
		}	
		
		if ($currentOrder && $currentOrder == 'ai_magic') {
			try{
			$customer_id = $this->_apiConfig->getClientId();    //Client ID
			$customer_key = $this->_apiConfig->getAccessToken();    //Access Token
			$optimise_type = 'View'; // Optimise Type


			$url = \Magento\Framework\App\ObjectManager::getInstance()
			->get('Magento\Framework\UrlInterface');

			$widget_type = "plp";
			$widget_instance = "Product List";
			$page_url = $url->getCurrentUrl();

			// Validating the Credentials
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "http://app.boxx.ai/client/validate/?client_id=".$customer_id."&access_token=".$customer_key."&primary_transaction_type=".$optimise_type."",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
			  return $result;
			}
			
			$responseArray=json_decode($response,true);
			
			}catch (\Exception $e) {
				return $result;
			}catch (\Error $e) {
				return $result;
			}
			
			if($responseArray['data']['valid'])
			{	
				try{
					$getAllProductIds = $collection->getAllIds();
					//recommendation request data query
					if($this->customerSession->isLoggedIn()) {
						$customerId = $this->customerSession->getCustomer()->getId();
						$this->_recommendationRequestDataQuery->setUserid($customerId);
					}else{
					  if($this->_session->getBoxxTokenId()){
						$this->_recommendationRequestDataQuery->setBoxxTokenId($this->_session->getBoxxTokenId());
					  }
					}
					$perPagelimit=$collection->getPageSize()?$collection->getPageSize():count($collection->getData());
					$this->_recommendationRequestDataQuery->setProductIds($getAllProductIds);
					$this->_recommendationRequestDataQuery->setPageNum($collection->getCurPage());
					$this->_recommendationRequestDataQuery->setNum($perPagelimit);
					//recommendation request data
					$this->_recommendationRequestData->setClientId($responseArray['data']['client_id']);
					$this->_recommendationRequestData->setChannelId($responseArray['data']['channel_id']);
					$this->_recommendationRequestData->setAccessToken($responseArray['data']['access_token']);
					$this->_recommendationRequestData->setQuery($this->_recommendationRequestDataQuery);
										//Tracking Data
					$this->_recommendationRequestData->setWidgetInstance($widget_instance);
					$this->_recommendationRequestData->setWidgetType($widget_type);
					$this->_recommendationRequestData->setPageUrl($page_url);
					$recommendedProducts = array();
					$resultset  = array();
				}catch (\Exception $e) {
					return $result;
				}catch (\Error $e) {
					return $result;
				}
				
				try {
					error_log('call rootPost');
					$resultset = $this->_recommendationApi->rootPost($this->_recommendationRequestData);
					if(!empty($resultset->getResult() )){
						foreach($resultset->getResult() as $key => $value){ 
						array_push($recommendedProducts,$value->id);
					 }
					}
					else
					{
						return $result;
					}
				}catch (\Exception $e) {
					return $result;
				}					
				if(!empty($recommendedProducts)){  
					try{
						error_log(print_r($recommendedProducts,1));
						$page = $this->request->getParam('p');
						error_log($page);
						$order_string = implode(',',array_reverse($recommendedProducts));
						$subject->getCollection()->getSelect()->reset('order');
						$subject->getCollection()->getSelect()->order(new \Zend_Db_Expr('FIELD(e.entity_id, ' .$order_string.') DESC'));
						$subject->setPageSize(count($getAllProductIds));
						$subject->setCurPage($page);
						return $subject;
					}catch (\Exception $e) {
						return $result;
					}catch (\Error $e) {
						return $result;
					}
				}
				else
				{
					return $result;
				}
			}
			else
			{
				return $result;
			}
		}
		else
		{
			 return $result;
		}       
    }
}

}
